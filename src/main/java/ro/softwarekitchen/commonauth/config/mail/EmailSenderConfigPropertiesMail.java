package ro.softwarekitchen.commonauth.config.mail;

import lombok.Data;

@Data
public class EmailSenderConfigPropertiesMail {
    private EmailSenderConfigPropertiesMailSmtp smtp;
}
