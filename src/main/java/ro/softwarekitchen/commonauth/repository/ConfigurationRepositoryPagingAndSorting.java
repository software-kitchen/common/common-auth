package ro.softwarekitchen.commonauth.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ro.softwarekitchen.commonauth.models.ConfigurationItem;
import ro.softwarekitchen.commonauth.dtos.ConfigurationFilterDTO;

@Repository
public interface ConfigurationRepositoryPagingAndSorting extends PagingAndSortingRepository<ConfigurationItem, Long> {
    @Query("select s from ConfigurationItem s where " +
        " (:#{#filterDTO.confKey} is null or lower(s.confKey) like lower(concat('%',:#{#filterDTO.confKey},'%')))"
    )
    Page<ConfigurationItem> findFiltered(ConfigurationFilterDTO filterDTO, PageRequest of);
}
