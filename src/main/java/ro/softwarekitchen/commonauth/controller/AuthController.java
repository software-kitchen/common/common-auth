package ro.softwarekitchen.commonauth.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ro.softwarekitchen.common_core.exception.ItemNotFoundException;
import ro.softwarekitchen.common_core.exception.WithMessageException;
import ro.softwarekitchen.commonauth.mail.EmailDetails;
import ro.softwarekitchen.commonauth.mail.EmailService;
import ro.softwarekitchen.commonauth.models.Role;
import ro.softwarekitchen.commonauth.models.User;
import ro.softwarekitchen.commonauth.payloads.request.ChangePasswordRequest;
import ro.softwarekitchen.commonauth.payloads.request.ForgotPasswordRequest;
import ro.softwarekitchen.commonauth.payloads.request.LoginRequest;
import ro.softwarekitchen.commonauth.payloads.request.RefreshTokenRequest;
import ro.softwarekitchen.commonauth.payloads.request.ResetPasswordRequest;
import ro.softwarekitchen.commonauth.payloads.request.SignupRequest;
import ro.softwarekitchen.commonauth.payloads.response.MessageResponse;
import ro.softwarekitchen.commonauth.payloads.response.UserInfoResponse;
import ro.softwarekitchen.commonauth.payloads.response.UserRights;
import ro.softwarekitchen.commonauth.repository.RoleRepository;
import ro.softwarekitchen.commonauth.repository.UserRepository;
import ro.softwarekitchen.commonauth.security.jwt.JwtUtils;
import ro.softwarekitchen.commonauth.security.permissions.RightsCache;
import ro.softwarekitchen.commonauth.security.permissions.RolesCache;
import ro.softwarekitchen.commonauth.services.UserDetailsImpl;
import ro.softwarekitchen.commonauth.services.UserService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/auth")
@AllArgsConstructor
@Slf4j
public class AuthController {

    private final AuthenticationManager authenticationManager;

    private final UserRepository userRepository;

    private final UserDetailsService userDetailsService;

    private final UserService userService;

    private final RoleRepository roleRepository;

    private final PasswordEncoder encoder;

    private final JwtUtils jwtUtils;

    private final RightsCache rightsCache;

    private final RolesCache rolesCache;

    private final EmailService emailService;

    @Operation(summary = "Get logged in user", description = "Returns logged in user or guest if there is no active session")
    @GetMapping()
    public ResponseEntity<UserInfoResponse> getAuthenticateUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ResponseEntity.ok().body(this.getUserInfo(authentication));
    }

    @Operation(summary = "Login", description = "Checks user credentials and performs login mechanism")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successfully logged in")})
    @PostMapping("/signin")
    public ResponseEntity<Object> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication =
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        verifyPinCode(userDetails, loginRequest.getPinCode());

        ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails.getUsername());
        ResponseCookie jwtRefreshCookie = jwtUtils.generateJwtRefreshCookie(userDetails.getUsername());
        ResponseCookie sessionCookie = jwtUtils.generateSessionCookie();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(HttpHeaders.SET_COOKIE, jwtCookie.toString());
        responseHeaders.add(HttpHeaders.SET_COOKIE, jwtRefreshCookie.toString());
        responseHeaders.add(HttpHeaders.SET_COOKIE, sessionCookie.toString());

        return ResponseEntity.ok().headers(responseHeaders).body(this.getUserInfo(authentication));
    }

    private void verifyPinCode(UserDetailsImpl userDetails, String pinCode) {
        User user = userRepository.findById(userDetails.getId()).orElseThrow(ItemNotFoundException::new);
        // null or empty
        if (StringUtils.isEmpty(user.getPinCode()) && StringUtils.isEmpty(pinCode)) {
            return;
        }
        // equal
        if (pinCode != null && user.getPinCode() != null && user.getPinCode().trim().equals(pinCode.trim())) {
            return;
        }
        throw new BadCredentialsException("PIN code incorrect !");
    }

    @Operation(summary = "Signup", description = "Add a new user, provide name, password, email and role")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "User registered successfully!")})
    @PostMapping("/signup")
    public ResponseEntity<MessageResponse> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (Boolean.TRUE.equals(userRepository.existsByUsername(signUpRequest.getUsername()))) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
        }

        if (Boolean.TRUE.equals(userRepository.existsByEmail(signUpRequest.getEmail()))) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRoles();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName("ROLE_USER").orElseThrow(() -> new RuntimeException("Error: ROLE_USER not defined"));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                Role assignedRole =
                    roleRepository.findByName(role).orElseThrow(() -> new RuntimeException("Error: Role: " + role + " is not found."));
                roles.add(assignedRole);
            });
        }
        user.setRoles(roles);

        if (!StringUtils.isEmpty(signUpRequest.getFirstName())) {
            user.setFirstName(signUpRequest.getFirstName());
        }
        if (!StringUtils.isEmpty(signUpRequest.getLastName())) {
            user.setLastName(signUpRequest.getLastName());
        }
        if (!StringUtils.isEmpty(signUpRequest.getTelephone())) {
            user.setTelephone(signUpRequest.getTelephone());
        }
        if (signUpRequest.getEnabled() != null) {
            user.setEnabled(signUpRequest.getEnabled());
        } else {
            user.setEnabled(true);
        }
        user.setResource(true);
        if (!StringUtils.isEmpty(signUpRequest.getPicture())) {
            user.setPicture(signUpRequest.getPicture());
        }
        if (!StringUtils.isEmpty(signUpRequest.getGender())) {
            user.setGender(signUpRequest.getGender());
        }
        if (!StringUtils.isEmpty(signUpRequest.getAddress())) {
            user.setAddress(signUpRequest.getAddress());
        }
        user.setTitle("");
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @Operation(summary = "Signout", description = "Logout")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "You've been signed out!")})
    @PostMapping("/signout")
    public ResponseEntity<MessageResponse> logoutUser() {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(HttpHeaders.SET_COOKIE, jwtUtils.resetJwtCookie().toString());
        responseHeaders.add(HttpHeaders.SET_COOKIE, jwtUtils.resetJwtRefreshCookie().toString());
        responseHeaders.add(HttpHeaders.SET_COOKIE, jwtUtils.resetsessionCookie().toString());
        return ResponseEntity.ok().headers(responseHeaders).body(new MessageResponse("You've been signed out!"));
    }

    @Operation(summary = "Reset password", description = "Reset password based on received token from /forgot_password endpoint")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "You have successfully changed your password.")})
    @PostMapping("/reset_password")
    public ResponseEntity<MessageResponse> processResetPassword(@Valid @RequestBody ResetPasswordRequest resetPasswordRequest) {
        String token = resetPasswordRequest.getToken();
        String password = resetPasswordRequest.getPassword();

        User user = userRepository.findByResetPasswordToken(token);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new MessageResponse("Invalid Token"));
        } else {
            userService.updatePassword(user, password);
            return ResponseEntity.ok(new MessageResponse("You have successfully changed your password."));
        }
    }

    @Operation(summary = "Change password", description = "Change password for current user")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "You have successfully changed your password.")})
    @PostMapping("/change_password")
    public ResponseEntity<MessageResponse> processChangePassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
        // get current user
        String userName = userService.getCurrentUser().getUsername();
        // verify password
        Authentication authentication =
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, changePasswordRequest.getCurrentPassword()));
        User user = userRepository.findByUsername(userName).orElseThrow(() -> new RuntimeException("Username: " + userName + " not found"));
        userService.updatePassword(user, changePasswordRequest.getNewPassword());
        return ResponseEntity.ok(new MessageResponse("You have successfully changed your password."));

    }

    @Operation(summary = "Forgot password", description = "Generate a token and send it to email in order to activate the link to reset the password")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "The reset Token")})
    @PostMapping("/forgot_password")
    public ResponseEntity<MessageResponse> processForgotPassword(@RequestBody ForgotPasswordRequest forgotPasswordRequest) {

        String token = RandomStringUtils.random(30, true, true);
        String email = forgotPasswordRequest.getEmail();
        try {
            userService.updateResetPasswordToken(token, email);
            String resetPasswordLink = ServletUriComponentsBuilder.fromCurrentContextPath().toUriString() + "/reset_password?token=" + token;
            EmailDetails emailDetails = new EmailDetails();
            emailDetails.setRecipient(email);
            emailDetails.setSubject("Reset password");
            emailDetails.setMsgBody(resetPasswordLink);
            emailService.sendSimpleMail(emailDetails);
            return ResponseEntity.ok(new MessageResponse(token));
        } catch (UsernameNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("User not found"));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new MessageResponse("Error while sending email"));
        }

    }

    @Operation(summary = "Refresh JwtToken", description = "Generate a new JwtToken")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "New JwtToken generated")})
    @PostMapping("/refreshJwtToken")
    public ResponseEntity<MessageResponse> refreshJwtToken(@RequestBody RefreshTokenRequest refreshTokenRequest) {

        String refreshToken = refreshTokenRequest.getRefreshToken();
        if (jwtUtils.validateJwtToken(refreshToken)) {
            String username = jwtUtils.getUserNameFromJwtToken(refreshToken);

            ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(username);
            ResponseCookie jwtRefreshCookie = jwtUtils.generateJwtRefreshCookie(username);
            ResponseCookie sessionCookie = jwtUtils.generateSessionCookie();

            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add(HttpHeaders.SET_COOKIE, jwtCookie.toString());
            responseHeaders.add(HttpHeaders.SET_COOKIE, jwtRefreshCookie.toString());
            responseHeaders.add(HttpHeaders.SET_COOKIE, sessionCookie.toString());

            return ResponseEntity.ok().headers(responseHeaders).body(new MessageResponse(jwtCookie.getValue()));
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new MessageResponse("Invalid Token"));
    }

    private UserInfoResponse getUserInfo(Authentication authentication) {
        try {
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

            List<String> roles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList();

            rightsCache.evictPermissions();
            rolesCache.evictRoles();
            UserRights userRights = rightsCache.getPermissions(userDetails.getUsername());

            return new UserInfoResponse(userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles, userRights.getPermissions(),
                userRights.getExtras());
        } catch (Exception e) {
            // we are guest
            return new UserInfoResponse(0L, "guest", "", null, null, null);
        }

    }

}
