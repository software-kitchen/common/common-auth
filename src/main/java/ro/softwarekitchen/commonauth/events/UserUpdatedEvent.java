package ro.softwarekitchen.commonauth.events;

import org.springframework.context.ApplicationEvent;

public class UserUpdatedEvent extends ApplicationEvent {
    private long userId;

    public UserUpdatedEvent(Object source, long userId) {
        super(source);
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }
}