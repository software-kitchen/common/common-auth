package ro.softwarekitchen.commonauth.mail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailDetailsExtended {
    private String recipient;

    private String recipientCc;

    private String recipientBcc;

    private String msgBody;

    private String subject;

    private String templateName;

    private List<String> attachments;

    private List<EmailInlineImage> inlineImages;

    // here the context
    private Object object;
}