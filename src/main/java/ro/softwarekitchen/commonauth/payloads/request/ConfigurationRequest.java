package ro.softwarekitchen.commonauth.payloads.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.Map;

@Data
public class ConfigurationRequest {
    @NotBlank
    private String confKey;

    private Map<String, Object> confValue;

}
