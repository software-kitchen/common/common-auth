package ro.softwarekitchen.commonauth.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.commonauth.config.mail.MailSenderProperties;
import ro.softwarekitchen.commonauth.mail.EmailDetails;
import ro.softwarekitchen.commonauth.mail.EmailDetailsExtended;
import ro.softwarekitchen.commonauth.mail.EmailService;

@RestController
@RequestMapping("/mail")
@RequiredArgsConstructor
public class EmailController {

    private final EmailService emailService;

    @Operation(summary = "Send simple email", description = "Send email without attachment")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Email sent successfully")
    })
    @PostMapping("/sendMail")
    @PreAuthorize("hasPermission('CONFIG', 'WRITE')")
    public ResponseEntity<String> sendMail(@RequestBody EmailDetails details) {
        return ResponseEntity.ok(emailService.sendSimpleMail(details));
    }

    @PostMapping("/sendAsyncMail")
    @PreAuthorize("hasPermission('CONFIG', 'WRITE')")
    public ResponseEntity<String> sendAsyncMail(@RequestBody EmailDetails details) {
        emailService.sendAsyncSimpleMail(details);
        return ResponseEntity.ok("email sent");
    }

    @Operation(summary = "Send html email", description = "Send html email with or without attachment")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Email sent successfully")
    })
    @PostMapping("/sendHtmlMail")
    @PreAuthorize("hasPermission('CONFIG', 'WRITE')")
    public ResponseEntity<String> sendHtmlMail(@RequestBody EmailDetails details) {
        return ResponseEntity.ok(emailService.sendMail(details));
    }

    @Operation(summary = "Send html email with object as context", description = "Send html email with or without attachment with object as context")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Email sent successfully")
    })
    @PostMapping("/sendHtmlMailExtended")
    @PreAuthorize("hasPermission('CONFIG', 'WRITE')")
    public ResponseEntity<String> sendHtmlEmailExtended(@RequestBody EmailDetailsExtended emailDetailsExtended) {
        return ResponseEntity.ok(emailService.sendHtmlEmailExtended(emailDetailsExtended));
    }

    @PostMapping("/sendAsyncHtmlMail")
    @PreAuthorize("hasPermission('CONFIG', 'WRITE')")
    public ResponseEntity<String> sendAsyncHtmlMail(@RequestBody EmailDetails details) {
        emailService.sendAsyncMail(details);
        return ResponseEntity.ok("email sent");
    }

    @GetMapping("/properties")
    @PreAuthorize("hasPermission('CONFIG', 'READ')")
    public ResponseEntity<MailSenderProperties> getEmailProperties() {
        return ResponseEntity.ok(emailService.getEmailProperties());
    }

}