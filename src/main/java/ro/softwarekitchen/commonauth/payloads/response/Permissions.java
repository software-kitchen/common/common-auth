package ro.softwarekitchen.commonauth.payloads.response;

import lombok.Data;

@Data
public class Permissions {
    private String module;

    private String rights;

    public Permissions(RawPermissions rawPermissions) {
        this.module = rawPermissions.getModule();
        this.rights = (rawPermissions.isRightToCreate() ? "1" : "0")
            + (rawPermissions.isRightToRead() ? "1" : "0")
            + (rawPermissions.isRightToWrite() ? "1" : "0")
            + (rawPermissions.isRightToDelete() ? "1" : "0");
    }
}
