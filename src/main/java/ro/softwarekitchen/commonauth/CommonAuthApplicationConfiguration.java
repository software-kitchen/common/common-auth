package ro.softwarekitchen.commonauth;

import com.corundumstudio.socketio.ClientOperations;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;
import ro.softwarekitchen.common_core.CommonCoreConfiguration;
import ro.softwarekitchen.commonauth.audit.AuditorAwareImpl;
import ro.softwarekitchen.commonauth.mail.DatabaseTemplateResolver;
import ro.softwarekitchen.commonauth.security.users.ActiveUserStore;

@Configuration
@EnableConfigurationProperties
@ComponentScan
@EnableAsync
@EnableJpaRepositories
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
@EnableScheduling
@Import({CommonCoreConfiguration.class})
public class CommonAuthApplicationConfiguration {

    private final SocketIOServer server;

    @Value("${spring.mail.templates.directory:src/main/resources/templates/}")
    private String templatesDirectory;

    public CommonAuthApplicationConfiguration(SocketIOServer server) {
        this.server = server;
    }

    @Bean
    public static ApplicationContextProvider contextProvider() {
        return new ApplicationContextProvider();
    }

    @Bean
    AuditorAware<String> auditorProvider() {
        return new AuditorAwareImpl();
    }

    @Bean
    public FileTemplateResolver directoryTemplateResolver() {
        FileTemplateResolver secondaryTemplateResolver = new FileTemplateResolver();
        secondaryTemplateResolver.setPrefix(templatesDirectory);
        secondaryTemplateResolver.setSuffix(".html");
        secondaryTemplateResolver.setTemplateMode(TemplateMode.HTML);
        secondaryTemplateResolver.setCharacterEncoding("UTF-8");
        secondaryTemplateResolver.setOrder(1);
        secondaryTemplateResolver.setCheckExistence(true);
        secondaryTemplateResolver.setCacheable(false);

        return secondaryTemplateResolver;
    }

    @Bean
    public DatabaseTemplateResolver databaseTemplateResolver() {
        DatabaseTemplateResolver databaseTemplateResolver = new DatabaseTemplateResolver();
        databaseTemplateResolver.setOrder(2);
        databaseTemplateResolver.setCheckExistence(true);
        databaseTemplateResolver.setCacheable(false);
        return databaseTemplateResolver;
    }

    @Bean
    public StringTemplateResolver stringTemplateResolver() {
        StringTemplateResolver stringTemplateResolver = new StringTemplateResolver();
        stringTemplateResolver.setOrder(3);
        stringTemplateResolver.setCheckExistence(true);
        stringTemplateResolver.setCacheable(false);
        return stringTemplateResolver;
    }

    @Bean(name = "applicationEventMulticaster")
    public ApplicationEventMulticaster simpleApplicationEventMulticaster() {
        SimpleApplicationEventMulticaster eventMulticaster = new SimpleApplicationEventMulticaster();

        eventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return eventMulticaster;
    }

    @Bean
    public ActiveUserStore activeUserStore() {
        return new ActiveUserStore();
    }

    @PreDestroy
    public void destroy() {
        // used to gracefully shutdown spring app
        if (this.server != null) {
            // disconnecting all clients
            this.server.getAllClients().stream().filter(SocketIOClient::isChannelOpen).forEach(ClientOperations::disconnect);
            // stopping the server
            this.server.stop();
        }

    }

}
