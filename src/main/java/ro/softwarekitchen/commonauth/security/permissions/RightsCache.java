package ro.softwarekitchen.commonauth.security.permissions;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import ro.softwarekitchen.commonauth.models.ModulePermissions;
import ro.softwarekitchen.commonauth.models.Role;
import ro.softwarekitchen.commonauth.models.User;
import ro.softwarekitchen.commonauth.payloads.response.Permissions;
import ro.softwarekitchen.commonauth.payloads.response.RawPermissions;
import ro.softwarekitchen.commonauth.payloads.response.UserRights;
import ro.softwarekitchen.commonauth.repository.RoleRepository;
import ro.softwarekitchen.commonauth.repository.UserRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@AllArgsConstructor
public class RightsCache {

    private static final String PK = "PermissionsKey";

    private final RoleRepository roleRepository;

    private final UserRepository userRepository;

    private final UserDetailsService userDetailsService;

    @Cacheable(PK)
    public UserRights getPermissions(String userName) {

        UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

        User user = userRepository.findByUsername(userDetails.getUsername()).orElseThrow();

        //        List<String> roles = userDetails.getAuthorities().stream()
        //            .map(GrantedAuthority::getAuthority)
        //            .toList();
        List<String> roles = user.getRoles().stream().map(Role::getName).toList();

        Map<String, RawPermissions> rawPermissionsMap = new HashMap<>();
        for (String roleName : roles) {
            Role role = roleRepository.findByName(roleName).orElseThrow(() -> new RuntimeException("Error: " + roleName + " not defined"));
            for (ModulePermissions modulePermission : role.getModulePermissions()) {
                RawPermissions rawPermissions = rawPermissionsMap.get(modulePermission.getModule().getName());
                if (rawPermissions == null) {
                    rawPermissions = new RawPermissions();
                    rawPermissions.setModule(modulePermission.getModule().getName());
                }
                rawPermissions.setRightToCreate(rawPermissions.isRightToCreate() || modulePermission.isRightToCreate());
                rawPermissions.setRightToRead(rawPermissions.isRightToRead() || modulePermission.isRightToRead());
                rawPermissions.setRightToWrite(rawPermissions.isRightToWrite() || modulePermission.isRightToWrite());
                rawPermissions.setRightToDelete(rawPermissions.isRightToDelete() || modulePermission.isRightToDelete());
                rawPermissionsMap.put(modulePermission.getModule().getName(), rawPermissions);
            }
        }
        return new UserRights(rawPermissionsMap.values().stream().map(Permissions::new).toList(), user.getExtras());
    }

    @CacheEvict(value = PK, allEntries = true)
    public void evictPermissions() {
        log.debug("Permissions cache evicted !");
    }
}
