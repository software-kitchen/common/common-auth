
## Name
common-auth 
- SpringBoot library with Security (oauth2), Swagger, email, MySQL connector, Users, Roles, Login management, health and micrometer/prometheus

## Description
basic oauth authentication, expose endpoints for user management/sigin/signup/etc

## Usage
- add in pom.xml
```		
<dependency>
    <groupId>ro.software-kitchen</groupId>
    <artifactId>common-auth</artifactId>
    <version>0.0.1</version>
</dependency>
...
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/48220762/packages/maven</url>
    </repository>
</repositories>

<distributionManagement>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/48220762/packages/maven</url>
    </repository>

    <snapshotRepository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/48220762/packages/maven</url>
    </snapshotRepository>
</distributionManagement>
	
```

- add in spring app
```
@Import(CommonAuthApplicationConfiguration.class)
```

- configure your application.yml file like the included one

- endpoints:
```http://localhost:1971/swagger-ui/index.html#/```
```http://localhost:1971/actuator```
```http://localhost:1971/actuator/prometheus```

- how to run

Assure you have mysql server and create a database with the same name as specified in application.yml
Start the app
Signup and create first user (as admin) using swagger ```http://localhost:1971/swagger-ui/index.html#/``` or postman
POST ```http://localhost:1971/api/auth/signup```
with payload 
```{
  "username": "your_user_name",
  "email": "valid_email_to_be_able_to_restore_passwd",
  "role": [
    "ROLE_ADMIN"
  ],
  "password": "desired _password"
}
```

By default, some paths are accessible without login. Please be free to update them in ```WebSecurityConfig```