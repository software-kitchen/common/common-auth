package ro.softwarekitchen.commonauth.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MemoryDetailDTO {
    private Integer processors;

    private double cpuUsage;

    private Long jvmFreeMemoryMB;

    private Long jvmMaxMemoryMB;

    private Long jvmTotalMemoryMB;

    private Long osTotalMemoryMB;

    private Long osFreeMemoryMB;

    private Long osUsedMemoryMB;

    private double memoryUsage;

}
