package ro.softwarekitchen.commonauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.softwarekitchen.commonauth.models.ConfigurationItem;

import java.util.Optional;

@Repository
public interface ConfigurationRepository extends JpaRepository<ConfigurationItem, Long> {
    Optional<ConfigurationItem> findByConfKey(String name);
}
