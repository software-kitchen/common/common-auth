package ro.softwarekitchen.commonauth.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@ConfigurationProperties(prefix = "web")
public class WebConfigProperties {

    private final Cors cors;

    public WebConfigProperties(Cors cors) {
        this.cors = cors;
    }

    @Getter
    public static class Cors {
        private final String[] allowedOrigins;

        private final String[] allowedMethods;

        private final String[] allowedHeaders;

        private final String[] exposedHeaders;

        private final long maxAge;

        private final boolean allowCredentials;

        public Cors(String[] allowedOrigins, String[] allowedMethods, long maxAge,
                    String[] allowedHeaders, String[] exposedHeaders, boolean allowCredentials) {
            this.allowedOrigins = allowedOrigins;
            this.allowedMethods = allowedMethods;
            this.maxAge = maxAge;
            this.allowedHeaders = allowedHeaders;
            this.exposedHeaders = exposedHeaders;
            this.allowCredentials = allowCredentials;
        }

    }
}
