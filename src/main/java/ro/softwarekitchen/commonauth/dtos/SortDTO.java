package ro.softwarekitchen.commonauth.dtos;

import lombok.Data;

@Data
public class SortDTO {
    private String fieldName;

    private Boolean ascending;
}
