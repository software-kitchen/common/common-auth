package ro.softwarekitchen.commonauth.dtos;

import lombok.Data;

@Data
public class UserReorderDTO {
    private Long id;

    private Integer order1;

    private Integer order2;
}
