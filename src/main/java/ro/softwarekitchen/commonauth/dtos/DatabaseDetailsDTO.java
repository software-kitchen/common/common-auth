package ro.softwarekitchen.commonauth.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;

@Data
@NoArgsConstructor
public class DatabaseDetailsDTO {
    private List<DatabaseTableDTO> tables;

    private Instant lastAccess;
}
