package ro.softwarekitchen.commonauth.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.commonauth.dtos.DatabaseDetailsDTO;
import ro.softwarekitchen.commonauth.dtos.DiskDetailDTO;
import ro.softwarekitchen.commonauth.dtos.MemoryDetailDTO;
import ro.softwarekitchen.commonauth.dtos.OsLoadDetailDTO;
import ro.softwarekitchen.commonauth.services.SystemInfoService;

import java.util.List;

@RestController
@RequestMapping("/system-information")
@RequiredArgsConstructor
@Slf4j
public class SystemInfoController {

    private final SystemInfoService systemInfoService;

    @Operation(summary = "Get database details", description = "Get database tables details")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Database tables details")
    })
    @GetMapping("/database")
    @PreAuthorize("hasPermission('CONFIG', 'READ')")
    public ResponseEntity<DatabaseDetailsDTO> getDatabaseDetails() {
        DatabaseDetailsDTO detailsDTO = new DatabaseDetailsDTO();
        detailsDTO.setTables(systemInfoService.getTablesDetails());
        detailsDTO.setLastAccess(systemInfoService.getLastAccess());
        return ResponseEntity.ok(detailsDTO);
    }

    @Operation(summary = "Get disk details", description = "Get disk details")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Disk details")
    })
    @GetMapping("/disk")
    @PreAuthorize("hasPermission('CONFIG', 'READ')")
    public ResponseEntity<List<DiskDetailDTO>> getDiskDetails() {
        return ResponseEntity.ok(systemInfoService.getDiskDetails());
    }

    @Operation(summary = "Get memory details", description = "Get memory details")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Memory details")
    })
    @GetMapping("/memory")
    @PreAuthorize("hasPermission('CONFIG', 'READ')")
    public ResponseEntity<MemoryDetailDTO> getMemoryDetail() {
        return ResponseEntity.ok(systemInfoService.getMemoryDetail());
    }

    @Operation(summary = "Get system load (max 6 hours)", description = "Get system load (max 6 hours)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "System load (max 6 hours)")
    })
    @GetMapping("/system-load")
    @PreAuthorize("hasPermission('CONFIG', 'READ')")
    public ResponseEntity<List<OsLoadDetailDTO>> getOsLoadDetail() {
        return ResponseEntity.ok(systemInfoService.getOsLoadDetailDTOS());
    }

}