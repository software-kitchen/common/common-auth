package ro.softwarekitchen.commonauth.events;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CommonAuthEventPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    public void publishUserUpdatedEvent(final long userId) {
        System.out.println("Publishing userUpdateEvent");
        UserUpdatedEvent userUpdatedEvent = new UserUpdatedEvent(this, userId);
        applicationEventPublisher.publishEvent(userUpdatedEvent);
    }
}