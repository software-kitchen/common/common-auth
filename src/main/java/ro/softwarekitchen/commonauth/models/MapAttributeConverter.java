package ro.softwarekitchen.commonauth.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.AttributeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * Attribute converter for converting maps to JSON strings, so that they can be saved in string columns
 * https://stackoverflow.com/questions/44308167/how-to-map-a-mysql-json-column-to-a-java-entity-property-using-jpa-and-hibernate
 */

public class MapAttributeConverter implements AttributeConverter<Map<String, Object>, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MapAttributeConverter.class);

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(final Map<String, Object> map) {
        try {
            return objectMapper.writeValueAsString(map);
        } catch (final JsonProcessingException e) {
            LOGGER.error("Could not convert map '{}' to string column!", map, e);
        }

        return null;
    }

    @Override
    public Map<String, Object> convertToEntityAttribute(final String jsonString) {
        try {
            return objectMapper.readValue(jsonString, Map.class);
        } catch (final IOException e) {
            LOGGER.error("Could not convert JSON '{}' to map!", jsonString, e);
        }

        return null;
    }

}
