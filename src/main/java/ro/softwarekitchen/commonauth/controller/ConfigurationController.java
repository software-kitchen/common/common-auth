package ro.softwarekitchen.commonauth.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.commonauth.models.ConfigurationItem;
import ro.softwarekitchen.commonauth.dtos.ConfigurationFilterDTO;
import ro.softwarekitchen.commonauth.payloads.request.ConfigurationRequest;
import ro.softwarekitchen.commonauth.payloads.response.MessageResponse;
import ro.softwarekitchen.commonauth.repository.ConfigurationRepository;
import ro.softwarekitchen.commonauth.repository.ConfigurationRepositoryPagingAndSorting;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/configuration")
@AllArgsConstructor
public class ConfigurationController {
    private final ConfigurationRepository configurationRepository;

    private final ConfigurationRepositoryPagingAndSorting configurationRepositoryPagingAndSorting;

    @PostMapping()
    @Operation(summary = "Create new", description = "Create new")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "New configuration")
    })
    @PreAuthorize("hasPermission('CONFIG', 'CREATE')")
    public ResponseEntity<?> create(@Valid @RequestBody ConfigurationRequest request) {
        if (configurationRepository.findByConfKey(request.getConfKey()).isPresent()) {
            return ResponseEntity.badRequest().body(new MessageResponse("Already defined"));
        }
        ConfigurationItem configuration = new ConfigurationItem();
        configuration.setConfKey(request.getConfKey());
        configuration.setConfValue(request.getConfValue());
        return ResponseEntity.ok(configurationRepository.save(configuration));
    }

    @PutMapping()
    @Operation(summary = "Update", description = "Update")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Update configuration")
    })
    @PreAuthorize("hasPermission('CONFIG', 'WRITE')")
    public ResponseEntity<?> update(@Valid @RequestBody ConfigurationRequest request) {
        Optional<ConfigurationItem> optionalConfiguration = configurationRepository.findByConfKey(request.getConfKey());
        if (optionalConfiguration.isEmpty()) {
            return ResponseEntity.badRequest().body(new MessageResponse("Configuration does not exists"));
        }
        ConfigurationItem configuration = optionalConfiguration.get();
        configuration.setConfValue(request.getConfValue());
        return ResponseEntity.ok(configurationRepository.save(configuration));
    }

    @GetMapping
    @Operation(summary = "Get all", description = "List all")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List")
    })
    @PreAuthorize("hasPermission('CONFIG', 'READ')")
    public ResponseEntity<List<ConfigurationItem>> getAll() {
        return ResponseEntity.ok(configurationRepository.findAll());
    }

    @GetMapping("/{key}")
    @Operation(summary = "Get by key", description = "Get by key")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "By key")
    })
    @PreAuthorize("hasPermission('CONFIG', 'READ')")
    public ResponseEntity<ConfigurationItem> getByKey(@PathVariable String key) {
        return ResponseEntity.ok(configurationRepository.findByConfKey(key).orElseThrow(() -> new RuntimeException("Error: Configuration with key: " + key + " is not found.")));
    }

    @DeleteMapping("/{key}")
    @Operation(summary = "Delete by key", description = "Delete by key")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Delete by key")
    })
    @PreAuthorize("hasPermission('CONFIG', 'DELETE')")
    public ResponseEntity<MessageResponse> deleteByKey(@PathVariable String key) {
        Optional<ConfigurationItem> optionalConfiguration = configurationRepository.findByConfKey(key);
        if (optionalConfiguration.isEmpty()) {
            return ResponseEntity.badRequest().body(new MessageResponse("Configuration does not exists"));
        }
        configurationRepository.delete(optionalConfiguration.get());
        return ResponseEntity.ok().body(new MessageResponse("Configuration successfully deleted"));
    }

    @PostMapping(value = "/filtered", params = {"page", "size"})
    @Operation(summary = "Get paged", description = "List of (paged)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List (paged)")
    })
    @PreAuthorize("hasPermission('CONFIG', 'READ')")
    public Page<ConfigurationItem> findAllPaged(@RequestParam(name = "page", defaultValue = "0", required = false) Integer page,
        @RequestParam(name = "size", defaultValue = "100", required = false) Integer size, @RequestBody ConfigurationFilterDTO configurationFilterDTO) {
        return configurationRepositoryPagingAndSorting.findFiltered(configurationFilterDTO, PageRequest.of(page, size));
    }

}
