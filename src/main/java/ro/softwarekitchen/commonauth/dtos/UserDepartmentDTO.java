package ro.softwarekitchen.commonauth.dtos;

import lombok.Data;

@Data
public class UserDepartmentDTO {

    private Long userId;

    private Long departmentId;

    private Integer costAllocationPercent;
}
