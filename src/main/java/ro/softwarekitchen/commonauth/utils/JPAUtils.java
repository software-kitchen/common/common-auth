package ro.softwarekitchen.commonauth.utils;

import io.micrometer.common.util.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import ro.softwarekitchen.commonauth.dtos.SortDTO;

public class JPAUtils {

    private JPAUtils() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static PageRequest calculatePageRequest(SortDTO sortDTO, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        if (sortDTO != null && !StringUtils.isEmpty(sortDTO.getFieldName())) {
            Sort sort = Sort.by(sortDTO.getFieldName());
            if (sortDTO.getAscending() != null && !sortDTO.getAscending()) {
                sort = sort.descending();
            } else {
                sort = sort.ascending();
            }
            pageRequest = PageRequest.of(page, size, sort);
        }
        return pageRequest;
    }
}
