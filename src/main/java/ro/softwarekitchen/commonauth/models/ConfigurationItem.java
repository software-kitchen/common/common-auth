package ro.softwarekitchen.commonauth.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditOverride;
import org.hibernate.envers.AuditOverrides;
import org.hibernate.envers.Audited;
import ro.softwarekitchen.commonauth.audit.Auditable;

import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@Table(name = "configuration")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id")
@Audited
@AuditOverrides(value = {
    @AuditOverride(forClass = ConfigurationItem.class),
    @AuditOverride(forClass = Auditable.class),
    @AuditOverride(forClass = Auditable.class, name = "creationDate", isAudited = false),
    @AuditOverride(forClass = Auditable.class, name = "lastModifiedDate", isAudited = false),
    @AuditOverride(forClass = Auditable.class, name = "createdBy", isAudited = false)
})
public class ConfigurationItem extends Auditable<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "conf_key")
    private String confKey;

    @Column(name = "conf_value", columnDefinition = "TEXT")
    @Convert(converter = MapAttributeConverter.class)
    private Map<String, Object> confValue = new HashMap<>();

}
