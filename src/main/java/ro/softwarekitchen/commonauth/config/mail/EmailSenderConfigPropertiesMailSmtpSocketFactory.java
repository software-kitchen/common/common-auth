package ro.softwarekitchen.commonauth.config.mail;

import lombok.Data;

@Data
public class EmailSenderConfigPropertiesMailSmtpSocketFactory {
    private String className;
}
