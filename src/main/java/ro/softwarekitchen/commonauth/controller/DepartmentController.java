package ro.softwarekitchen.commonauth.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.common_core.exception.ItemNotFoundException;
import ro.softwarekitchen.common_core.exception.WithMessageException;
import ro.softwarekitchen.commonauth.controller.crud.CrudController;
import ro.softwarekitchen.commonauth.models.Department;
import ro.softwarekitchen.commonauth.repository.UserDepartmentRepository;
import ro.softwarekitchen.commonauth.services.DepartmentService;

@RestController
@RequestMapping("/departments")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class DepartmentController extends CrudController<Department, Long> {

    private final UserDepartmentRepository userDepartmentRepository;

    public DepartmentController(DepartmentService service, UserDepartmentRepository userDepartmentRepository) {
        super(service);
        this.userDepartmentRepository = userDepartmentRepository;
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Department create(@RequestBody Department entity) {
        return getService().create(entity);
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> delete(Long id) {
        Department department = getService().getById(id).orElseThrow(ItemNotFoundException::new);
        if (!userDepartmentRepository.findByDepartment(department).isEmpty()) {
            throw new WithMessageException("Departamentul este folosit !");
        }
        return super.delete(id);
    }

}
