package ro.softwarekitchen.commonauth.controller;

import jakarta.websocket.server.PathParam;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.commonauth.meterregistry.MicrometerService;

@RestController
@RequestMapping(path = "/micrometer", produces = MediaType.APPLICATION_JSON_VALUE)
public class MicrometerController {
    private final MicrometerService micrometerService;

    public MicrometerController(MicrometerService micrometerService) {
        this.micrometerService = micrometerService;
    }

    @PostMapping("/gaugeWithBuilder")
    public String updateGaugeWithBuilder(@PathParam("Value") int value) {
        int currentValue = micrometerService.getGaugeValueForBuilder().intValue();
        micrometerService.getGaugeValueForBuilder().set(currentValue + value);
        return "ok";
    }

    @PostMapping("/gauge")
    public String updateGauge(@PathParam("Value") int value) {
        int currentValue = micrometerService.getGaugeValue().intValue();
        micrometerService.getGaugeValue().set(currentValue + value);
        return "ok";
    }
}
