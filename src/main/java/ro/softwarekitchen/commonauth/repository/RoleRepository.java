package ro.softwarekitchen.commonauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ro.softwarekitchen.commonauth.models.Role;
import ro.softwarekitchen.commonauth.payloads.request.RoleModulePermissionsRequest;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(String name);
}
