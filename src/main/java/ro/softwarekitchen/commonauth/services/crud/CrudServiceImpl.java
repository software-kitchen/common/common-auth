package ro.softwarekitchen.commonauth.services.crud;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class CrudServiceImpl<T, ID> implements CrudService<T, ID> {

    private final CrudRepository<T, ID> repository;

    public CrudServiceImpl(CrudRepository<T, ID> repository) {
        this.repository = repository;
    }

    @Override
    public Iterable<T> getAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsById(ID id) {
        return repository.existsById(id);
    }

    @Override
    public Optional<T> getById(ID id) {
        return repository.findById(id);
    }

    @Override
    public T create(T entity) {
        return repository.save(entity);
    }

    @Override
    public T update(ID id, T entity) {
        if (!repository.existsById(id)) {
            throw new RuntimeException("Resource not found with id: " + id);
        }
        return repository.save(entity);
    }

    @Override
    public void delete(ID id) {
        repository.deleteById(id);
    }

    public Page<T> getPaged(PageRequest pageRequest) {
        List<T> models = new ArrayList<>();
        repository.findAll().forEach(models::add);

        final int pStart = (int) pageRequest.getOffset();
        final int pEnd = Math.min((pStart + pageRequest.getPageSize()), models.size());
        if (pStart > pEnd) {
            return new PageImpl<>(List.of(), pageRequest, models.size());
        }
        return new PageImpl<>(models.subList(pStart, pEnd), pageRequest, models.size());
    }
}