package ro.softwarekitchen.commonauth.config.mail;

import lombok.Data;

@Data
public class EmailSenderConfigPropertiesMailSmtpStartTls {
    private String enable;

    private String required;
}
