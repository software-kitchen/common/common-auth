package ro.softwarekitchen.commonauth.scheduler;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ro.softwarekitchen.commonauth.services.SystemInfoService;

@Service
@RequiredArgsConstructor
public class SystemInfoScheduler {

    private final SystemInfoService systemInfoService;

    // run every minute
    @Scheduled(cron = "0 * * * * ?")
    private void readAndStoreSystemLoad(){
        systemInfoService.readAndStoreSystemLoad();
    }
}
