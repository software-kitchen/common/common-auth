package ro.softwarekitchen.commonauth.services;

import com.corundumstudio.socketio.SocketIOClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.softwarekitchen.commonauth.socket.Message;
import ro.softwarekitchen.commonauth.socket.MessageType;

@Service
@Slf4j
public class SocketService {

    public void sendMessage(String room, String eventName, SocketIOClient senderClient, String message) {
        for (
            SocketIOClient client : senderClient.getNamespace().getRoomOperations(room).getClients()) {
            log.info("Client session: {}, Sender session: {}", client.getSessionId(), senderClient.getSessionId());
            // test for ChatApp ... not necessary here
            //if (!client.getSessionId().equals(senderClient.getSessionId())) {
            client.sendEvent(eventName, new Message(MessageType.NOTIFICATION, message));
            //}
        }
    }

}
