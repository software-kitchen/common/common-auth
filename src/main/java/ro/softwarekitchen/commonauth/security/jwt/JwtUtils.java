package ro.softwarekitchen.commonauth.security.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Component;
import org.springframework.web.util.WebUtils;

import java.util.Date;

@Component
@Slf4j
public class JwtUtils {

    private static final String SESSION_COOKIE_NAME = "session";

    private static final String SESSION_COOKIE_VALUE = "10000";

    @Value("${application.jwtSecret}")
    private String jwtSecret;

    @Value("${application.jwtExpirationMs}")
    private int jwtExpirationMs;

    @Value("${application.jwtRefreshExpirationMs}")
    private int jwtRefreshExpirationMs;

    @Value("${application.jwtCookieExpirationMs}")
    private int jwtCookieExpirationMs;

    @Value("${application.jwtCookieName}")
    private String jwtCookie;

    @Value("${application.jwtSameSite}")
    private String jwtSameSite;

    @Value("${application.jwtSecure}")
    private boolean jwtSecure;

    public String getJwtFromCookies(HttpServletRequest request) {
        Cookie cookie = WebUtils.getCookie(request, jwtCookie);
        if (cookie != null) {
            return cookie.getValue();
        } else {
            return null;
        }
    }

    public ResponseCookie generateJwtCookie(String userPrincipal) {
        String jwt = generateTokenFromUsername(userPrincipal, jwtExpirationMs);
        return ResponseCookie.from(jwtCookie, jwt).path("/").maxAge(jwtCookieExpirationMs / 1000).httpOnly(true).sameSite(jwtSameSite).secure(jwtSecure).build();
    }

    public ResponseCookie generateJwtRefreshCookie(String userPrincipal) {
        String jwt = generateTokenFromUsername(userPrincipal, jwtRefreshExpirationMs);
        return ResponseCookie.from(jwtCookie + "Refresh", jwt).path("/").maxAge(jwtCookieExpirationMs / 1000).httpOnly(true).sameSite(jwtSameSite).secure(jwtSecure).build();
    }

    public ResponseCookie generateSessionCookie() {
        return ResponseCookie.from(SESSION_COOKIE_NAME, SESSION_COOKIE_VALUE).path("/").sameSite(jwtSameSite).secure(jwtSecure).build();
    }

    public ResponseCookie resetJwtCookie() {
        return ResponseCookie.from(jwtCookie, null).path("/").sameSite(jwtSameSite).secure(jwtSecure).build();
    }

    public ResponseCookie resetJwtRefreshCookie() {
        return ResponseCookie.from(jwtCookie + "Refresh", null).path("/").sameSite(jwtSameSite).secure(jwtSecure).build();
    }

    public ResponseCookie resetsessionCookie() {
        return ResponseCookie.from(SESSION_COOKIE_NAME, null).path("/").sameSite(jwtSameSite).secure(jwtSecure).build();
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).build().parseSignedClaims(token).getPayload().getSubject();
    }

    public boolean validateJwtToken(String authToken) {
        if (authToken == null || authToken.isEmpty()) {
            return false;
        }
        try {
            Jwts.parser().setSigningKey(jwtSecret).build().parseSignedClaims(authToken).getPayload();
            return true;
        } catch (SignatureException e) {
            log.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            log.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }

    public String generateTokenFromUsername(String username) {
        return generateTokenFromUsername(username, jwtExpirationMs);
    }

    private String generateTokenFromUsername(String username, int expirationMs) {
        String token = Jwts.builder()
            .setSubject(username)
            .setIssuedAt(new Date())
            .setExpiration(new Date((new Date()).getTime() + expirationMs))
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .compact();
        log.debug("Token: {}", token);
        return token;
    }

}
