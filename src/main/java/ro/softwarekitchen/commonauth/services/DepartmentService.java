package ro.softwarekitchen.commonauth.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.softwarekitchen.commonauth.models.Department;
import ro.softwarekitchen.commonauth.repository.DepartmentRepository;
import ro.softwarekitchen.commonauth.services.crud.CrudServiceImpl;

@Service
@Slf4j
public class DepartmentService extends CrudServiceImpl<Department, Long> {

    public DepartmentService(DepartmentRepository repository) {
        super(repository);
    }

}
