package ro.softwarekitchen.commonauth.payloads.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class UserInfoResponse {
    private Long id;

    private String username;

    private String email;

    private List<String> roles;

    private List<Permissions> permissions;

    private String extras;
}
