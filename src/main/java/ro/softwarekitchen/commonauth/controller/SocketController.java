package ro.softwarekitchen.commonauth.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.commonauth.socket.Message;
import ro.softwarekitchen.commonauth.socket.SocketModule;

@RestController
@RequestMapping("/socket")
@RequiredArgsConstructor
public class SocketController {

    private final SocketModule socketModule;

    @PostMapping("/send")
    @ApiOperation(value = "Send message")
    public void createTask(@RequestBody Message message) {
        socketModule.sendMessage(message.getRoom(), message.getMessage());
    }

    @GetMapping("/")
    @PreAuthorize("hasPermission('EMAIL', 'WRITE')")
    public String get() {
        return "ok";
    }
}
