package ro.softwarekitchen.commonauth.security.users;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;

@Data
@AllArgsConstructor
public class UserRequest {
    private String uri;

    private String method;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserRequest that = (UserRequest) o;
        return uri.equals(that.uri) && method.equals(that.method);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.uri + this.method);
    }
}
