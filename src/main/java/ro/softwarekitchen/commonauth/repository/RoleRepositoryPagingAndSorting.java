package ro.softwarekitchen.commonauth.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ro.softwarekitchen.commonauth.models.Role;
import ro.softwarekitchen.commonauth.dtos.RoleFilterDTO;

@Repository
public interface RoleRepositoryPagingAndSorting extends PagingAndSortingRepository<Role, Long> {
    @Query("select s from Role s where " +
        " (:#{#filterDTO.name} is null or lower(s.name) like lower(concat('%',:#{#filterDTO.name},'%')))"
    )
    Page<Role> findFiltered(RoleFilterDTO filterDTO, PageRequest of);
}
