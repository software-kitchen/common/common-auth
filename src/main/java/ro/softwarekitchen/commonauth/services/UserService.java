package ro.softwarekitchen.commonauth.services;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ro.softwarekitchen.commonauth.models.User;
import ro.softwarekitchen.commonauth.dtos.UserDepartmentDTO;

import java.util.List;

public interface UserService {

    void updateResetPasswordToken(String token, String email) throws UsernameNotFoundException;

    User getByResetPasswordToken(String token);

    void updatePassword(User user, String newPassword);

    User getCurrentUser();

    User updateDepartments(Long userId, List<UserDepartmentDTO> dtos);
}

