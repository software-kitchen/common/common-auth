package ro.softwarekitchen.commonauth.health;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * sk is the id used in application configuration. For other Health classed use the same notation YourClassName-HealthIndicator for class and yourClassName for id in configuration file
 */
@Component
public class SkHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        // define your own logic
        return Health.up().build();
    }
}
