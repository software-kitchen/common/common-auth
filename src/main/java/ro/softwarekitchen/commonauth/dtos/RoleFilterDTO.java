package ro.softwarekitchen.commonauth.dtos;

import lombok.Data;

@Data
public class RoleFilterDTO {
    private String name;
}
