package ro.softwarekitchen.commonauth.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import ro.softwarekitchen.commonauth.security.SKPermissionEvaluator;
import ro.softwarekitchen.commonauth.security.permissions.RightsCache;
import ro.softwarekitchen.commonauth.security.permissions.RolesCache;

@Configuration
@EnableMethodSecurity
@AllArgsConstructor
public class MethodSecurityConfig {

    private final RightsCache rightsCache;

    private final RolesCache rolesCache;

    @Bean
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        DefaultMethodSecurityExpressionHandler expressionHandler =
                new DefaultMethodSecurityExpressionHandler();
        expressionHandler.setPermissionEvaluator(new SKPermissionEvaluator(rightsCache, rolesCache));
        return expressionHandler;
    }
}