package ro.softwarekitchen.commonauth.audit;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import ro.softwarekitchen.commonauth.services.UserDetailsImpl;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {

        return Optional.ofNullable(SecurityContextHolder.getContext())
            .map(SecurityContext::getAuthentication)
            .filter(Authentication::isAuthenticated)
            .map(Authentication::getPrincipal)
            .map(UserDetailsImpl.class::cast)
            .map(UserDetailsImpl::getUsername);
        //
        //        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        //        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        //        if (userDetails != null) {
        //            return Optional.ofNullable(userDetails.getUsername());
        //        }
        //        return Optional.ofNullable("-");
    }

}