package ro.softwarekitchen.commonauth.mail;

import org.thymeleaf.context.Context;
import ro.softwarekitchen.commonauth.config.mail.MailSenderProperties;

// https://www.geeksforgeeks.org/spring-boot-sending-email-via-smtp/
public interface EmailService {

    String sendSimpleMail(EmailDetails details);

    void sendAsyncSimpleMail(EmailDetails details);

    String sendMail(EmailDetails details);

    void sendAsyncMail(EmailDetails details);

    String sendHtmlEmailWithContext(EmailDetails details, Context context);

    void sendAsyncHtmlEmailWithContext(EmailDetails details, Context context);

    MailSenderProperties getEmailProperties();

    String sendHtmlEmailExtended(EmailDetailsExtended emailDetailsExtended);
}