package ro.softwarekitchen.commonauth.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.commonauth.models.Module;
import ro.softwarekitchen.commonauth.models.ModulePermissions;
import ro.softwarekitchen.commonauth.payloads.request.ModulePermissionsRequest;
import ro.softwarekitchen.commonauth.repository.ModulePermissionsRepository;
import ro.softwarekitchen.commonauth.repository.ModuleRepository;
import ro.softwarekitchen.commonauth.security.permissions.RightsCache;
import ro.softwarekitchen.commonauth.security.permissions.RolesCache;

import java.util.List;

@RestController
@RequestMapping("/module-permissions")
@AllArgsConstructor
public class ModulePermissionsController {

    private final ModulePermissionsRepository modulePermissionsRepository;

    private final ModuleRepository moduleRepository;

    private final RightsCache rightsCache;

    private final RolesCache rolesCache;

    @GetMapping
    @PreAuthorize("hasPermission('CONFIG', 'Read')")
    @Operation(summary = "Get all module permissions", description = "List of all modules and permissions")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List of module permissions")
    })
    public ResponseEntity<List<ModulePermissions>> getAll() {
        return ResponseEntity.ok(modulePermissionsRepository.findAll());
    }

    @GetMapping("/{mpId}")
    @PreAuthorize("hasPermission('CONFIG', 'Read')")
    @Operation(summary = "Get module permissions by id", description = "Module permissions by id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Module permissions")
    })
    public ResponseEntity<ModulePermissions> getModulePermissionById(@PathVariable long mpId) {
        return ResponseEntity.ok(modulePermissionsRepository.findById(mpId).orElseThrow());
    }

    @PostMapping
    @PreAuthorize("hasPermission('CONFIG', 'Write')")
    @Operation(summary = "Add module permissions", description = "Add module permissions")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Module permissions")
    })
    public ResponseEntity<ModulePermissions> createModulePermission(@RequestBody ModulePermissionsRequest modulePermissionsRequest) {
        Module module = moduleRepository.findById(modulePermissionsRequest.getModuleId()).orElseThrow();
        ModulePermissions modulePermissions = new ModulePermissions();
        modulePermissions.setModule(module);
        modulePermissions.setRightToCreate(modulePermissionsRequest.isRightToCreate());
        modulePermissions.setRightToRead(modulePermissionsRequest.isRightToRead());
        modulePermissions.setRightToWrite(modulePermissionsRequest.isRightToWrite());
        modulePermissions.setRightToDelete(modulePermissionsRequest.isRightTDelete());
        rightsCache.evictPermissions();
        rolesCache.evictRoles();
        return ResponseEntity.ok(modulePermissionsRepository.save(modulePermissions));
    }

    @PutMapping("/{mpId}")
    @PreAuthorize("hasPermission('CONFIG', 'Write')")
    @Operation(summary = "Update module permissions", description = "Update module permissions")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Module permissions")
    })
    public ResponseEntity<ModulePermissions> updateModulePermission(@PathVariable("mpId") Long mpId, @RequestBody ModulePermissionsRequest modulePermissionsRequest) {
        ModulePermissions modulePermissions = modulePermissionsRepository.getReferenceById(mpId);
        modulePermissions.setRightToCreate(modulePermissionsRequest.isRightToCreate());
        modulePermissions.setRightToRead(modulePermissionsRequest.isRightToRead());
        modulePermissions.setRightToWrite(modulePermissionsRequest.isRightToWrite());
        modulePermissions.setRightToDelete(modulePermissionsRequest.isRightTDelete());
        rightsCache.evictPermissions();
        rolesCache.evictRoles();
        return ResponseEntity.ok(modulePermissionsRepository.save(modulePermissions));
    }

    @DeleteMapping("/{mpId}")
    @PreAuthorize("hasPermission('CONFIG', 'Delete')")
    @Operation(summary = "Delete module permissions", description = "Delete module permissions")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Acknowledge")
    })
    public ResponseEntity<String> deleteModulePermission(@PathVariable("mpId") long mpId) {
        // i expect db will block deletion for linked ones
        if (modulePermissionsRepository.existsById(mpId)) {
            modulePermissionsRepository.deleteById(mpId);
            rightsCache.evictPermissions();
            rolesCache.evictRoles();
            return ResponseEntity.ok("ModulePermissions deleted");
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ModulePermissions with id:" + mpId + " does not exists");
    }

}
