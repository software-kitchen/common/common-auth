package ro.softwarekitchen.commonauth.config.mail;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import ro.softwarekitchen.commonauth.models.ConfigurationItem;
import ro.softwarekitchen.commonauth.repository.ConfigurationRepository;

import java.util.Map;
import java.util.Optional;
import java.util.Properties;

@Slf4j
@Configuration
@ConfigurationProperties(prefix = "spring.mail")
@RequiredArgsConstructor
@Data
public class EmailSenderConfig {

    private static final String SPRING_EMAIL_PROTOCOL = "spring.mail.protocol";

    private static final String SPRING_EMAIL_CONFIGURATION_SOURCE = "spring.mail.configuration.source";

    private final ConfigurationRepository configurationRepository;

    private String host;

    private String protocol;

    private String username;

    private String password;

    private String port;

    private EmailSenderConfigProperties properties;

    @Bean
    @Scope("prototype")
    public JavaMailSender getJavaMailSender() {

        // check from dbConfiguration
        Optional<ConfigurationItem> emailProperties = configurationRepository.findByConfKey("email");
        if (emailProperties.isPresent()) {
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            Map<String, Object> confValue = emailProperties.get().getConfValue();
            if (confValue != null) {

                log.info("Database Configuration properties used to initialize MailSender");

                if (confValue.get(SPRING_EMAIL_PROTOCOL) != null) {
                    mailSender.setProtocol(confValue.get(SPRING_EMAIL_PROTOCOL).toString());
                }
                if (confValue.get("spring.mail.host") != null) {
                    mailSender.setHost(confValue.get("spring.mail.host").toString());
                }
                if (confValue.get("spring.mail.port") != null) {
                    mailSender.setPort(Integer.parseInt(confValue.get("spring.mail.port").toString()));
                }
                if (confValue.get("spring.mail.username") != null) {
                    mailSender.setUsername(confValue.get("spring.mail.username").toString());
                }
                if (confValue.get("spring.mail.password") != null) {
                    mailSender.setPassword(confValue.get("spring.mail.password").toString());
                }

                Properties props = mailSender.getJavaMailProperties();

                props.put(SPRING_EMAIL_CONFIGURATION_SOURCE, "DATABASE");
                if (confValue.get(SPRING_EMAIL_PROTOCOL) != null) {
                    props.put(SPRING_EMAIL_PROTOCOL, confValue.get(SPRING_EMAIL_PROTOCOL).toString());
                }
                if (confValue.get("spring.mail.properties.mail.smtp.starttls.enable") != null) {
                    props.put("mail.smtps.starttls.enable", confValue.get("spring.mail.properties.mail.smtp.starttls.enable").toString());
                }
                if (confValue.get("spring.mail.properties.mail.smtp.starttls.required") != null) {
                    props.put("mail.smtps.starttls.required", confValue.get("spring.mail.properties.mail.smtp.starttls.required").toString());
                }
                if (confValue.get("spring.mail.properties.mail.smtp.auth") != null) {
                    props.put("mail.smtp.auth", confValue.get("spring.mail.properties.mail.smtp.auth").toString());
                }
                if (confValue.get("spring.mail.properties.mail.smtp.ssl.enable") != null) {
                    props.put("mail.smtp.ssl.enable", confValue.get("spring.mail.properties.mail.smtp.ssl.enable").toString());
                }
                if (confValue.get("spring.mail.properties.mail.smtp.socketFactory.class") != null) {
                    props.put("mail.smtp.socketFactory.class", confValue.get("spring.mail.properties.mail.smtp.socketFactory.class").toString());
                }

                return mailSender;
            }

        }

        log.info("Application Configuration File properties used to initialize MailSender");

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setProtocol(protocol);
        mailSender.setHost(host);
        mailSender.setPort(Integer.parseInt(port));
        mailSender.setUsername(username);
        mailSender.setPassword(password);

        Properties props = mailSender.getJavaMailProperties();
        props.put(SPRING_EMAIL_CONFIGURATION_SOURCE, "CONFIGURATION FILE");
        EmailSenderConfigPropertiesMailSmtp smtp = properties.getMail().getSmtp();
        props.put(SPRING_EMAIL_PROTOCOL, protocol);
        if (smtp != null && smtp.getStarttls() != null && smtp.getStarttls().getEnable() != null) {
            props.put("mail.smtps.starttls.enable", smtp.getStarttls().getEnable());
        }
        if (smtp != null && smtp.getStarttls() != null && smtp.getStarttls().getRequired() != null) {
            props.put("mail.smtps.starttls.required", smtp.getStarttls().getRequired());
        }
        if (smtp != null && smtp.getAuth() != null) {
            props.put("mail.smtp.auth", smtp.getAuth());
        }
        if (smtp != null && smtp.getSsl() != null && smtp.getSsl().getEnable() != null) {
            props.put("mail.smtp.ssl.enable", smtp.getSsl().getEnable());
        }

        if (smtp != null && smtp.getSocketFactory() != null && smtp.getSocketFactory().getClassName() != null) {
            props.put("mail.smtp.socketFactory.class", smtp.getSocketFactory().getClassName());
        } else {
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }

        return mailSender;
    }

}
