package ro.softwarekitchen.commonauth.config.mail;

import lombok.Data;

@Data
public class EmailSenderConfigPropertiesMailSmtpSsl {
    private String enable;
}
