package ro.softwarekitchen.commonauth.controller.crud;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ro.softwarekitchen.common_core.exception.PageNotFoundException;
import ro.softwarekitchen.commonauth.services.crud.CrudService;

import java.util.Optional;

public abstract class CrudController<T, ID> {

    private final CrudService<T, ID> service;

    protected CrudController(CrudService<T, ID> service) {
        this.service = service;
    }

    public CrudService<T, ID> getService() {
        return service;
    }

    @GetMapping("/all")
    public Iterable<T> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<T> getById(@PathVariable ID id) {
        Optional<T> optionalEntity = service.getById(id);
        return optionalEntity.map(entity -> new ResponseEntity<>(entity, HttpStatus.OK))
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(params = {"page", "size"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List of items")
    })
    public ResponseEntity<Page<T>> findPaginated(@RequestParam(name = "page", defaultValue = "0", required = false) Integer page,
        @RequestParam(name = "size", defaultValue = "100", required = false) Integer size) {
        Page<T> resultPage = service.getPaged(PageRequest.of(page, size));
        if (page > resultPage.getTotalPages()) {
            throw new PageNotFoundException(page);
        }
        return ResponseEntity.ok(resultPage);
    }

    @PostMapping
    public T create(@RequestBody T entity) {
        return service.create(entity);
    }

    @PutMapping("/{id}")
    public ResponseEntity<T> update(@PathVariable ID id, @RequestBody T entity) {
        service.update(id, entity);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<T> partialUpdate(@PathVariable ID id, @RequestBody T updates) {
        return service.getById(id)
            .map(entity -> {
                // Apply partial updates to entity here
                service.update(id, entity);
                return new ResponseEntity<>(entity, HttpStatus.OK);
            })
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable ID id) {
        if (!service.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}