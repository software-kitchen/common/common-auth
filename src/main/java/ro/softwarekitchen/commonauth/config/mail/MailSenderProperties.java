package ro.softwarekitchen.commonauth.config.mail;

import lombok.Data;

import java.util.Hashtable;

@Data
public class MailSenderProperties {
    private Hashtable<Object, Object> props;

    private String protocol;

    private String host;

    private String port;

    private String username;
}

