package ro.softwarekitchen.commonauth.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.commonauth.controller.crud.CrudController;
import ro.softwarekitchen.commonauth.models.EmailTemplate;
import ro.softwarekitchen.commonauth.services.EmailTemplateService;

@RestController
@RequestMapping("/mail-template")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class EmailTemplateController extends CrudController<EmailTemplate, Long> {
    public EmailTemplateController(EmailTemplateService service) {
        super(service);
    }
}
