package ro.softwarekitchen.commonauth.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.softwarekitchen.commonauth.models.EmailTemplate;
import ro.softwarekitchen.commonauth.repository.EmailTemplateRepository;
import ro.softwarekitchen.commonauth.services.crud.CrudServiceImpl;

@Service
@Slf4j
public class EmailTemplateService extends CrudServiceImpl<EmailTemplate, Long> {

    public EmailTemplateService(EmailTemplateRepository repository) {
        super(repository);
    }

}
