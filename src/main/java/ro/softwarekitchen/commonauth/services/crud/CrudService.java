package ro.softwarekitchen.commonauth.services.crud;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

public interface CrudService<T, ID> {
    Iterable<T> getAll();

    Optional<T> getById(ID id);

    boolean existsById(ID id);

    T create(T entity);

    T update(ID id, T entity);

    void delete(ID id);

    Page<T> getPaged(PageRequest pageRequest);
}

