package ro.softwarekitchen.commonauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class CommonAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommonAuthApplication.class, args);
	}

}
