package ro.softwarekitchen.commonauth.dtos;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

@Data
public class DatabaseTableDTO {
    private String table;

    private Long records;

    private BigDecimal sizeInMb;

    public DatabaseTableDTO(Map<String, Object> record) {
        table = record.get("Table").toString();
        records = (Long) record.get("Records");
        sizeInMb = (BigDecimal) record.get("SizeInMb");
    }
}
