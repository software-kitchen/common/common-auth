package ro.softwarekitchen.commonauth.mail;

import lombok.Data;

@Data
public class EmailInlineImage {
    private String imageName;

    private String imageFile;
}
