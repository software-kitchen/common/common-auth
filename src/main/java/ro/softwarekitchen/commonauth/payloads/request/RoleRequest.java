package ro.softwarekitchen.commonauth.payloads.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.springframework.lang.Nullable;
import ro.softwarekitchen.commonauth.models.ModulePermissions;

import java.util.Collection;
import java.util.Optional;

@Data
public class RoleRequest {
    @NotBlank
    private String name;

    @Nullable
    private Collection<ModulePermissions> modulePermissions;
}
