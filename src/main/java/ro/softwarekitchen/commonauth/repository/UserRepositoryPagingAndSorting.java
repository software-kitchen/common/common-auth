package ro.softwarekitchen.commonauth.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.softwarekitchen.commonauth.models.User;
import ro.softwarekitchen.commonauth.dtos.UserFilterDTO;

@Repository
public interface UserRepositoryPagingAndSorting extends PagingAndSortingRepository<User, Long> {
    @Query("select s from User s " +
        " left join s.userDepartments ud " +
        " where " +
        " (:#{#filterDTO.username} is null or lower(s.username) like lower(concat('%',:#{#filterDTO.username},'%')))" +
        " and (:#{#filterDTO.firstName} is null or lower(s.firstName) like lower(concat('%',:#{#filterDTO.firstName},'%')))" +
        " and (:#{#filterDTO.lastName} is null or lower(s.lastName) like lower(concat('%',:#{#filterDTO.lastName},'%')))" +
        " and (:#{#filterDTO.telephone} is null or lower(s.telephone) like lower(concat('%',:#{#filterDTO.telephone},'%')))" +
        " and (:#{#filterDTO.email} is null or lower(s.email) like lower(concat('%',:#{#filterDTO.email},'%')))" +
        " and (:#{#filterDTO.gender} is null or lower(s.gender) like lower(concat('%',:#{#filterDTO.gender},'%')))" +
        " and (:#{#filterDTO.address} is null or lower(s.address) like lower(concat('%',:#{#filterDTO.address},'%')))" +
        " and (:#{#filterDTO.departmentIds} is null or ud.department.id in :#{#filterDTO.departmentIds})" +
        " and " +
        " ( " +
        "   :#{#filterDTO.generalField} is null " +
        " or " +
        "   lower(s.username) like lower(concat('%',:#{#filterDTO.generalField},'%'))" +
        " or " +
        "   lower(s.firstName) like lower(concat('%',:#{#filterDTO.generalField},'%'))" +
        " or " +
        "   lower(s.lastName) like lower(concat('%',:#{#filterDTO.generalField},'%'))" +
        " or " +
        "   lower(s.email) like lower(concat('%',:#{#filterDTO.generalField},'%'))" +
        " ) "
    )
    Page<User> findFiltered(@Param("filterDTO") UserFilterDTO filterDTO, Pageable pageable);
}

