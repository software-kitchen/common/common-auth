package ro.softwarekitchen.commonauth.socket;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import io.netty.handler.codec.http.HttpHeaders;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ro.softwarekitchen.commonauth.security.jwt.JwtUtils;
import ro.softwarekitchen.commonauth.services.SocketService;
import ro.softwarekitchen.commonauth.services.UserDetailsServiceImpl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * HOW TO USE POSTMAN
 * WORKS ONLY FROM localhost
 * WebSockets module
 * Link ws://127.0.0.1:1973?room=a
 * params: room=a
 * Headers:
 * Cookie -> hdp-service=<jwt from login>
 * Settings
 * Client Version v2
 * Handshake Path /socket.io
 * Message
 * {
 * "room":"a",
 * "type":"WARNING",
 * "message":"Virgi1"
 * }
 * ack unchecked
 * send_message as name
 */
@Slf4j
@Component
public class SocketModule {

    private final SocketIOServer server;

    private final SocketService socketService;

    private final JwtUtils jwtUtils;

    private final UserDetailsServiceImpl userDetailsService;

    public Map<String, Set<SocketIOClient>> clients = new HashMap<>();

    @Value("${application.jwtCookieName}")
    private String jwtCookieName;

    public SocketModule(SocketIOServer server, SocketService socketService, JwtUtils jwtUtils, UserDetailsServiceImpl userDetailsService) {
        this.server = server;
        this.socketService = socketService;
        this.jwtUtils = jwtUtils;
        this.userDetailsService = userDetailsService;
        this.server.addConnectListener(onConnected());
        this.server.addDisconnectListener(onDisconnected());
        this.server.addEventListener("send_message", Message.class, onMessageReceived());
    }

    public void sendMessage(String room, String message) {
        if (clients.get(room) != null) {
            // send message to all clients from room
            clients.get(room).forEach(s -> socketService.sendMessage(room, "get_message", s, message));
        }
    }

    private DataListener<Message> onMessageReceived() {
        return (senderClient, data, ackSender) -> {
            log.info(data.toString());
        };
    }

    private ConnectListener onConnected() {
        return (client) -> {
            String room = client.getHandshakeData().getSingleUrlParam("room");
            // jwt should be in the cookie named like in configuration
            String jwt = getCookieValueFromHeader(client.getHandshakeData().getHttpHeaders(), jwtCookieName);
            if (jwt != null) {
                client.joinRoom(room);
                log.debug("Jwt: {}", jwt);
                if (validateJwt(jwt)) {
                    clients.computeIfAbsent(room, k -> new HashSet<>());
                    clients.get(room).add(client);
                    log.debug("Socket ID[{}]  Connected to socket", client.getSessionId().toString());
                } else {
                    log.info("Socket connection rejected. Invalid JWT");
                    client.disconnect();
                }
            } else {
                log.info("Socket connection rejected. No JWT provided");
                client.disconnect();
            }

        };

    }

    private DisconnectListener onDisconnected() {
        return client -> {
            log.debug("Client[{}] - Disconnected from socket", client.getSessionId().toString());
            // remove sessionId
            clients.forEach((k, v) -> log.info("Room: {}, Sessions: {}", k, v.size()));
            clients.forEach((room, existingClients) -> existingClients.removeIf(existingClient -> existingClient.getSessionId().equals(client.getSessionId())));
            clients.forEach((k, v) -> log.info("Room: {}, Sessions: {}", k, v.size()));
        };
    }

    private boolean validateJwt(String jwt) {
        if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
            String username = jwtUtils.getUserNameFromJwtToken(jwt);
            // validate username (exception will be thrown if user is not in db)
            return userDetailsService.loadUserByUsername(username).isEnabled();
        }
        return false;
    }

    private String getCookieValueFromHeader(HttpHeaders httpHeaders, String cookieName) {
        if (httpHeaders != null) {
            String cookies = httpHeaders.get("Cookie");
            if (cookies != null) {
                String[] cookieParts = cookies.split(";");
                for (String cookiePart : cookieParts) {
                    String[] cookieNameValue = cookiePart.trim().split("=");
                    if (cookieNameValue.length == 2) {
                        String name = cookieNameValue[0].trim();
                        if (name.equalsIgnoreCase(cookieName)) {
                            return cookieNameValue[1].trim();
                        }
                    }
                }
            }
        }

        return null;
    }
}
