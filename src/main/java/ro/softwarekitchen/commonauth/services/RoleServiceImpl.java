package ro.softwarekitchen.commonauth.services;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import ro.softwarekitchen.commonauth.models.ModulePermissions;
import ro.softwarekitchen.commonauth.models.Role;
import ro.softwarekitchen.commonauth.payloads.request.RoleModulePermissionsRequest;
import ro.softwarekitchen.commonauth.repository.ModulePermissionsRepository;
import ro.softwarekitchen.commonauth.repository.RoleRepository;
import ro.softwarekitchen.commonauth.security.permissions.RightsCache;
import ro.softwarekitchen.commonauth.security.permissions.RolesCache;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    private final ModulePermissionsRepository modulePermissionsRepository;

    private final RightsCache rightsCache;

    private final RolesCache rolesCache;

    @Override
    public Optional<Role> updatePermissions(RoleModulePermissionsRequest rpmRequest) {

        Long roleId = rpmRequest.getRoleId();
        if (roleId != null) {
            Role role = roleRepository.findById(roleId).orElseThrow();
            // delete existing permissions
            Collection<Long> existingPermissionsIds = role.getModulePermissions().stream().map(ModulePermissions::getId).collect(Collectors.toList());
            role.getModulePermissions().clear();
            roleRepository.save(role);
            modulePermissionsRepository.deleteAllById(existingPermissionsIds);
            // update
            if (!StringUtils.isEmpty(rpmRequest.getName())) {
                role.setName(rpmRequest.getName());
            }
            if (rpmRequest.getModulePermissions() != null) {
                role.setModulePermissions(rpmRequest.getModulePermissions());
            }
            rightsCache.evictPermissions();
            rolesCache.evictRoles();
            return Optional.of(roleRepository.save(role));

        }
        return Optional.empty();
    }

}
