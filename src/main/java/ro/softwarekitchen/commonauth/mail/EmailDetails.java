package ro.softwarekitchen.commonauth.mail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailDetails {
    private String recipient;

    private String recipientCc;

    private String recipientBcc;

    private String msgBody;

    private String subject;

    private String templateName;

    private String attachment;
}