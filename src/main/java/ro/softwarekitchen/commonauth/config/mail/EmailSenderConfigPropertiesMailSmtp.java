package ro.softwarekitchen.commonauth.config.mail;

import lombok.Data;

@Data
public class EmailSenderConfigPropertiesMailSmtp {
    private EmailSenderConfigPropertiesMailSmtpStartTls starttls;
    private String auth;
    private EmailSenderConfigPropertiesMailSmtpSsl ssl;
    private EmailSenderConfigPropertiesMailSmtpSocketFactory socketFactory;
}
