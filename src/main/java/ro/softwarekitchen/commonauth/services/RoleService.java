package ro.softwarekitchen.commonauth.services;

import org.springframework.http.ResponseEntity;
import ro.softwarekitchen.commonauth.models.Role;
import ro.softwarekitchen.commonauth.payloads.request.RoleModulePermissionsRequest;

import java.util.Optional;

public interface RoleService {

    Optional<Role> updatePermissions(RoleModulePermissionsRequest rpmRequest);

}

