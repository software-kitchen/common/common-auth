package ro.softwarekitchen.commonauth.config.mail;

import lombok.Data;

@Data
public class EmailSenderConfigProperties {
    private EmailSenderConfigPropertiesMail mail;
}
