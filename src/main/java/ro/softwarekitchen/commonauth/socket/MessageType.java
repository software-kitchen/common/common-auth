package ro.softwarekitchen.commonauth.socket;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

public enum MessageType {
    NOTIFICATION, WARNING, @JsonEnumDefaultValue UNKNOWN
}
