package ro.softwarekitchen.commonauth.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@AllArgsConstructor
public class OsLoadDetailDTO {

    private Instant timeStamp;

    private int percentageCpuUsage;

    private int percentageMemoryUsed;

    public OsLoadDetailDTO(MemoryDetailDTO memoryDetailDTO) {

        timeStamp = Instant.now();
        try {
            percentageCpuUsage = (int) (memoryDetailDTO.getCpuUsage() * 100);
            percentageMemoryUsed = (int) (memoryDetailDTO.getMemoryUsage() * 100);
        }catch(Exception e){
            percentageCpuUsage = 100;
            percentageMemoryUsed = 100;
        }
    }
}
