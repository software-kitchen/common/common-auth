package ro.softwarekitchen.commonauth.security;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

@Data
public class SkGrantedAuthority implements GrantedAuthority {

    private String module;

    private String rights;

    @Override
    public String getAuthority() {
        return module + ":" + rights;
    }
}
