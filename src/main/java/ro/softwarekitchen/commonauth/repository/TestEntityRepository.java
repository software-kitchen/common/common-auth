package ro.softwarekitchen.commonauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.softwarekitchen.commonauth.models.TestEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface TestEntityRepository extends JpaRepository<TestEntity, Long> {
    Optional<TestEntity> findByName(String name);

    List<TestEntity> findAllByName(String name);
}
