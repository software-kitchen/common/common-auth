package ro.softwarekitchen.commonauth.security.jwt;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import ro.softwarekitchen.commonauth.security.users.ActiveUserStore;
import ro.softwarekitchen.commonauth.security.users.UserActivities;
import ro.softwarekitchen.commonauth.security.users.UserRequest;
import ro.softwarekitchen.commonauth.services.UserDetailsServiceImpl;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;

@Slf4j
public class AuthTokenFilter extends OncePerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private ActiveUserStore activeUserStore;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
        throws ServletException, IOException {
        try {
            UserDetails userDetails = null;
            // check with jwt
            String jwt = parseJwt(request);
            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
                userDetails = userDetailsService.loadUserByUsername(jwtUtils.getUserNameFromJwtToken(jwt));
            }

            // check with authorisation - api key
            if (userDetails == null) {
                userDetails = userDetailsService.loadUserByApiKey(request.getHeader("Authorization"));
            }
            // try to login and set the user
            if (userDetails != null) {

                UsernamePasswordAuthenticationToken authentication =
                    new UsernamePasswordAuthenticationToken(userDetails,
                        null,
                        userDetails.getAuthorities());

                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
            // save in store
            try {
                if (userDetails != null) {
                    String userName = userDetails.getUsername();
                    UserActivities userActivities =
                        activeUserStore.getUserActivities().computeIfAbsent(userName, x -> new UserActivities(new HashMap<>()));
                    userActivities.getRequests().put(new UserRequest(request.getRequestURI(), request.getMethod()), Instant.now());
                }
            } catch (Exception e) {
                log.error("Failed to save in activeUserStore", e);
            }
        } catch (Exception e) {
            logger.error("Cannot set user authentication", e);
        }

        filterChain.doFilter(request, response);
    }

    private String parseJwt(HttpServletRequest request) {
        return jwtUtils.getJwtFromCookies(request);
    }
}
