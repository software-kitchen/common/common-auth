package ro.softwarekitchen.commonauth.security.permissions;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import ro.softwarekitchen.commonauth.services.UserDetailsImpl;

@Service
@Slf4j
@AllArgsConstructor
public class RolesCache {

    private static final String RK = "RolesKey";

    @Cacheable(RK)
    public boolean hasPrivilegeTroughRole(UserDetailsImpl userDetails, String roleName) {
        return userDetails.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .anyMatch(roleName::equals);

    }

    @CacheEvict(value = RK, allEntries = true)
    public void evictRoles() {
        log.debug("Roles cache evicted !");
    }
}
