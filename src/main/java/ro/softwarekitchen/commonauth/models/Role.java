package ro.softwarekitchen.commonauth.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Entity
@Table(name = "roles")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 20)
    private String name;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
        name = "role_module_permissions",
        joinColumns = @JoinColumn(
            name = "role_id"),
        inverseJoinColumns = @JoinColumn(
            name = "module_permission_id"))
    private Collection<ModulePermissions> modulePermissions;

}
