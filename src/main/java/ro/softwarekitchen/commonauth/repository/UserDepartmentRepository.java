package ro.softwarekitchen.commonauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.softwarekitchen.commonauth.models.Department;
import ro.softwarekitchen.commonauth.models.UserDepartment;

import java.util.List;

@Repository
public interface UserDepartmentRepository extends JpaRepository<UserDepartment, Long> {
    List<UserDepartment> findByDepartment(Department department);
}

