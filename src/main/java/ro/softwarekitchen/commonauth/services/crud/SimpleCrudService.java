package ro.softwarekitchen.commonauth.services.crud;

import java.util.List;
import java.util.Optional;

public interface SimpleCrudService<T> {
    List<T> findAll();

    Optional<T> findById(Long id);

    T insert(T t);

    T update(Long id, T t);

    void delete(Long id);
}
