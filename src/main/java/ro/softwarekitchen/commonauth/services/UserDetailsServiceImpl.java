package ro.softwarekitchen.commonauth.services;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.softwarekitchen.commonauth.models.User;
import ro.softwarekitchen.commonauth.repository.UserRepository;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username == null) {
            return null;
        }
        User user = userRepository.findByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        return UserDetailsImpl.build(user);
    }

    @Transactional
    public UserDetails loadUserByApiKey(String authorisation) throws UsernameNotFoundException {
        if (authorisation == null || !authorisation.startsWith("Basic ")) {
            return null;
        }
        String apiKey = authorisation.substring(6);
        User user = userRepository.findByApiKey(apiKey)
            .orElseThrow(() -> new UsernameNotFoundException("User Not Found with apiKey: " + apiKey));
        return UserDetailsImpl.build(user);
    }
}