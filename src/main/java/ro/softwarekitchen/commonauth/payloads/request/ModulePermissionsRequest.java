package ro.softwarekitchen.commonauth.payloads.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ModulePermissionsRequest {

    @NotNull
    private Long moduleId;

    @NotNull
    private boolean rightToCreate;

    @NotNull
    private boolean rightToRead;

    @NotNull
    private boolean rightToWrite;

    @NotNull
    private boolean rightTDelete;
}
