package ro.softwarekitchen.commonauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.softwarekitchen.commonauth.models.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    User findByEmail(String email);

    User findByResetPasswordToken(String token);

    List<User> findByOrderByLastNameAscFirstNameAsc();

    Optional<User> findByApiKey(String apiKey);
}

