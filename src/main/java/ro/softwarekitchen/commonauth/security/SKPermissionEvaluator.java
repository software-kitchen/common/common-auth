package ro.softwarekitchen.commonauth.security;

import lombok.AllArgsConstructor;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import ro.softwarekitchen.commonauth.payloads.response.Permissions;
import ro.softwarekitchen.commonauth.security.permissions.RightsCache;
import ro.softwarekitchen.commonauth.security.permissions.RolesCache;
import ro.softwarekitchen.commonauth.services.UserDetailsImpl;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
public class SKPermissionEvaluator implements PermissionEvaluator {

    private final RightsCache rightsCache;

    private final RolesCache rolesCache;

    @Override
    public boolean hasPermission(Authentication auth, Object targetDomainObject, Object permission) {
        if ((auth == null) || (targetDomainObject == null) || !(permission instanceof String)) {
            return false;
        }
        // String targetType = targetDomainObject.getClass().getSimpleName().toUpperCase();

        return hasPrivilege(auth, targetDomainObject.toString().toUpperCase(), permission.toString().toUpperCase());
    }

    @Override
    public boolean hasPermission(Authentication auth, Serializable targetId, String targetType, Object permission) {
        if ((auth == null) || (targetType == null) || !(permission instanceof String)) {
            return false;
        }
        return hasPrivilege(auth, targetType.toUpperCase(),
            permission.toString().toUpperCase());
    }

    private boolean hasPrivilege(Authentication auth, String targetName, String permission) {
        // Admin
        if (hasRole(auth, targetName, "ROLE_ADMIN")) {
            return true;
        }

        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
        if (!userDetails.isEnabled()) {
            return false;
        }

        List<Permissions> permissions = rightsCache.getPermissions(userDetails.getUsername()).getPermissions();
        // auth.getAuthorities() contains users roles
        //        for (GrantedAuthority grantedAuth : auth.getAuthorities()) {
        //            if (grantedAuth.getAuthority().startsWith(targetName) &&
        //                grantedAuth.getAuthority().contains(permission)) {
        //                return true;
        //            }
        //        }
        for (Permissions p : permissions) {
            if (p.getModule().equalsIgnoreCase(targetName)) {
                return hasRight(permission, p.getRights());
            }
        }
        return false;
    }

    private boolean hasRole(Authentication auth, String targetName, String roleName) {
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
        if (!userDetails.isEnabled()) {
            return false;
        }
        if (rolesCache.hasPrivilegeTroughRole(userDetails, roleName)) {
            return true;
        }
        return false;
    }

    private boolean hasAdminRole(Authentication auth, String targetName) {
        return hasRole(auth, targetName, "ROLE_ADMIN");
    }

    /**
     * @param permission - (CREATE, READ, WRITE, DELETE)
     * @param rights     - XXXX (x=1 or x=0)
     * @return boolean
     */
    boolean hasRight(String permission, String rights) {
        if ("CREATE".equals(permission) && rights.charAt(0) == '1')
            return true;
        if ("READ".equals(permission) && rights.charAt(1) == '1')
            return true;
        if ("WRITE".equals(permission) && rights.charAt(2) == '1')
            return true;
        if ("DELETE".equals(permission) && rights.charAt(3) == '1')
            return true;
        return false;
    }
}