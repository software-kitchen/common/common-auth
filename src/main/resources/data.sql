/* will be executed each start */

CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO roles (id, name)
    SELECT * FROM (SELECT 1, 'ROLE_ADMIN') AS tmp
        WHERE NOT EXISTS (
            SELECT name FROM roles WHERE name = 'ROLE_ADMIN'
        ) LIMIT 1;

INSERT INTO roles (id, name)
    SELECT * FROM (SELECT 2, 'ROLE_USER') AS tmp
        WHERE NOT EXISTS (
            SELECT name FROM roles WHERE name = 'ROLE_USER'
        ) LIMIT 1;


CREATE TABLE IF NOT EXISTS `modules` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO modules (id, name)
    SELECT * FROM (SELECT 1, 'CONFIG') AS tmp
        WHERE NOT EXISTS (
            SELECT name FROM modules WHERE name = 'CONFIG'
        ) LIMIT 1;
INSERT INTO modules (id, name)
    SELECT * FROM (SELECT 2, 'USERS') AS tmp
        WHERE NOT EXISTS (
            SELECT name FROM modules WHERE name = 'USERS'
        ) LIMIT 1;

CREATE TABLE IF NOT EXISTS `REVINFO` (
  `REV` int NOT NULL AUTO_INCREMENT,
  `REVTSTMP` bigint DEFAULT NULL,
  PRIMARY KEY (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE IF NOT EXISTS configuration (
    `id` int NOT NULL AUTO_INCREMENT,
    `conf_key` VARCHAR(255),
    `conf_value` TEXT,
    `created_date` DATETIME,
    `last_mod_date` DATETIME,
    `created_by` VARCHAR(50),
    `modified_by` VARCHAR(50),
    PRIMARY KEY (`id`)
);


INSERT IGNORE INTO configuration (id, conf_key , conf_value) values (1, 'main', '{"application.planning.working-hours-per-day":8,"application.planning.sunday-is-free":true,"application.planning.saturday-is-free":true}');
-- INSERT IGNORE INTO configuration (id, conf_key , conf_value) values (2, 'email', '{"spring.mail.protocol":"smtp","spring.mail.host":"185.104.183.50","spring.mail.sender":"softwarekitchen.ro@gmail.com","spring.mail.username":"","spring.mail.password":"", "spring.mail.port":"1025","spring.mail.properties.mail.smtp.starttls.enable":"false","spring.mail.properties.mail.smtp.auth":"false" }');


CREATE TABLE IF NOT EXISTS `configuration_audit` (
  `ID` bigint NOT NULL,
  `REVISION_ID` int NOT NULL,
  `REVISION_TYPE` tinyint DEFAULT NULL,
  `item_id` bigint DEFAULT NULL,
  `conf_key` VARCHAR(255) DEFAULT NULL,
  `conf_value` TEXT DEFAULT NULL,
  `modified_by` VARCHAR(50),
  PRIMARY KEY (`REVISION_ID`,`ID`),
  CONSTRAINT `configuration_audit_fk` FOREIGN KEY (`REVISION_ID`) REFERENCES `REVINFO` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
