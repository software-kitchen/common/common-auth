package ro.softwarekitchen.commonauth.services;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.query.sql.internal.NativeQueryImpl;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.stereotype.Service;
import ro.softwarekitchen.commonauth.dtos.DatabaseTableDTO;
import ro.softwarekitchen.commonauth.dtos.DiskDetailDTO;
import ro.softwarekitchen.commonauth.dtos.MemoryDetailDTO;
import ro.softwarekitchen.commonauth.dtos.OsLoadDetailDTO;

import java.nio.file.FileStore;
import java.nio.file.FileSystems;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SystemInfoService {

    private final static String SQL_TABLE_DETAILS = """
        SELECT
             table_name AS `Table`,
             table_rows AS `Records`,
             round(((data_length + index_length) / 1024 / 1024), 2) `SizeInMb`
        FROM information_schema.TABLES
        WHERE table_schema = DATABASE()
        ORDER BY (data_length + index_length) DESC;
        """;

    private final static String SQL_LAST_ACCESS = """
        select max(REVTSTMP) as lastAccess
            FROM REVINFO;
        """;

    @Getter
    private final List<OsLoadDetailDTO> osLoadDetailDTOS = new ArrayList<>();

    @PersistenceContext
    private EntityManager entityManager;

    public List<DatabaseTableDTO> getTablesDetails() {
        Query query = entityManager.createNativeQuery(SQL_TABLE_DETAILS);
        NativeQueryImpl<Map<String, Object>> nativeQuery = (NativeQueryImpl) query;
        nativeQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        List<Map<String, Object>> result = nativeQuery.getResultList();
        return result.stream().map(DatabaseTableDTO::new).toList();
    }

    public Instant getLastAccess() {
        Query query = entityManager.createNativeQuery(SQL_LAST_ACCESS);
        NativeQueryImpl<Map<String, Object>> nativeQuery = (NativeQueryImpl) query;
        nativeQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        List<Map<String, Object>> result = nativeQuery.getResultList();
        if (result.size() == 1 && result.get(0).get("lastAccess") != null) {
            return Instant.ofEpochMilli((Long) result.get(0).get("lastAccess"));
        } else {
            return null;
        }
    }

    public List<DiskDetailDTO> getDiskDetails() {
        List<DiskDetailDTO> list = new ArrayList<>();
        try {
            for (FileStore store : FileSystems.getDefault().getFileStores()) {
                try {
                    long total = store.getTotalSpace() / 1024 / 1024 / 1024;
                    long used = (store.getTotalSpace() - store.getUnallocatedSpace()) / 1024 / 1024 / 1024;
                    long avail = store.getUsableSpace() / 1024 / 1024 / 1024;
                    System.out.format("%-20s %12d %12d %12d%n", store, total, used, avail);
                    list.add(new DiskDetailDTO(store.name(), total, used, avail));
                } catch (Exception e) {
                    log.error("Error ", e);
                    list.add(new DiskDetailDTO(store.name() + " - error", 0L, 0L, 0L));
                }
            }
        } catch (Exception e1) {
            log.error("Error ", e1);
        }

        return list;
    }

    public MemoryDetailDTO getMemoryDetail() {
        com.sun.management.OperatingSystemMXBean bean =
            (com.sun.management.OperatingSystemMXBean)
                java.lang.management.ManagementFactory.getOperatingSystemMXBean();
        long osTotal = bean.getTotalMemorySize();
        long osFree = bean.getFreeMemorySize();

        double memoryUsed = ((double) osFree / (double) osTotal);
        memoryUsed = 1 - memoryUsed;

        double cpuUsage = bean.getSystemLoadAverage() / bean.getAvailableProcessors();
        return new MemoryDetailDTO(
            Runtime.getRuntime().availableProcessors(),
            cpuUsage,
            Runtime.getRuntime().freeMemory() / 1024 / 1024,
            Runtime.getRuntime().maxMemory() / 1024 / 1024,
            Runtime.getRuntime().totalMemory() / 1024 / 1024,
            osTotal / 1024 / 1024,
            osFree / 1024 / 1024,
            (osTotal - osFree) / 1024 / 1024,
            memoryUsed
        );

    }

    public void readAndStoreSystemLoad() {
        // cut the list to 6 hours (360 items)
        if (osLoadDetailDTOS.size() > 360) {
            osLoadDetailDTOS.remove(0);
        }
        osLoadDetailDTOS.add(new OsLoadDetailDTO(getMemoryDetail()));
    }
}
