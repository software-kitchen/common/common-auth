package ro.softwarekitchen.commonauth.payloads.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.lang.Nullable;
import ro.softwarekitchen.commonauth.models.ModulePermissions;

import java.util.Collection;

@Data
public class RoleModulePermissionsRequest {

    @Nullable
    private String name;

    @NotNull
    private Long roleId;

    @NotNull
    private Collection<ModulePermissions> modulePermissions;

}
