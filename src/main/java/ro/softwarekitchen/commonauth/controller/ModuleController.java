package ro.softwarekitchen.commonauth.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.commonauth.models.Module;
import ro.softwarekitchen.commonauth.repository.ModuleRepository;

import java.util.List;

@RestController
@RequestMapping("/modules")
@RequiredArgsConstructor
public class ModuleController {

    private final ModuleRepository moduleRepository;

    @Operation(summary = "Get modules", description = "List of modules")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List of modules")
    })
    @GetMapping
    @PreAuthorize("hasPermission('CONFIG', 'READ')")
    public List<Module> getUsers() {
        return moduleRepository.findAll();
    }
}