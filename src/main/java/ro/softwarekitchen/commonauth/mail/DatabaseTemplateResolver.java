package ro.softwarekitchen.commonauth.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.templateresolver.StringTemplateResolver;
import org.thymeleaf.templateresource.ITemplateResource;
import org.thymeleaf.templateresource.StringTemplateResource;
import ro.softwarekitchen.commonauth.models.EmailTemplate;
import ro.softwarekitchen.commonauth.repository.EmailTemplateRepository;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class DatabaseTemplateResolver extends StringTemplateResolver {
    private final static String PREFIX = "";

    @Autowired
    private EmailTemplateRepository repository;

    public DatabaseTemplateResolver() {
        setResolvablePatterns(Set.of("*"));
    }

    @Override
    protected ITemplateResource computeTemplateResource(IEngineConfiguration configuration, String ownerTemplate, String template, Map<String, Object> templateResolutionAttributes) {
        List<EmailTemplate> emailTemplates = repository.findByName(template);
        if (emailTemplates.isEmpty()) {
            return null;
        }
        return new StringTemplateResource(emailTemplates.get(0).getContent());
    }

}