package ro.softwarekitchen.commonauth.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.common_core.exception.GeneralException;
import ro.softwarekitchen.commonauth.dtos.UserDepartmentDTO;
import ro.softwarekitchen.commonauth.dtos.UserFilterDTO;
import ro.softwarekitchen.commonauth.dtos.UserReorderDTO;
import ro.softwarekitchen.commonauth.events.CommonAuthEventPublisher;
import ro.softwarekitchen.commonauth.models.User;
import ro.softwarekitchen.commonauth.models.UserDepartment;
import ro.softwarekitchen.commonauth.payloads.request.UserRequest;
import ro.softwarekitchen.commonauth.payloads.response.MessageResponse;
import ro.softwarekitchen.commonauth.repository.DepartmentRepository;
import ro.softwarekitchen.commonauth.repository.RoleRepository;
import ro.softwarekitchen.commonauth.repository.UserRepository;
import ro.softwarekitchen.commonauth.repository.UserRepositoryPagingAndSorting;
import ro.softwarekitchen.commonauth.security.users.ActiveUserStore;
import ro.softwarekitchen.commonauth.security.users.UserActivities;
import ro.softwarekitchen.commonauth.services.UserService;
import ro.softwarekitchen.commonauth.utils.JPAUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    private final UserRepository userRepository;

    private final UserRepositoryPagingAndSorting userRepositoryPagingAndSorting;

    private final PasswordEncoder encoder;

    private final RoleRepository roleRepository;

    private final DepartmentRepository departmentRepository;

    private final CommonAuthEventPublisher commonAuthEventPublisher;

    private final ActiveUserStore activeUserStore;

    @Operation(summary = "Get users", description = "List of users (admin role mandatory)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List of users")
    })
    @GetMapping
    @PreAuthorize("hasPermission('USERS', 'READ')")
    public List<User> getUsers() {
        return userRepository.findByOrderByLastNameAscFirstNameAsc().stream().filter(user -> user.isEnabled() && user.isResource()).map(User::reduce)
            .toList();
    }

    @Operation(summary = "Get user by id", description = "Get user by id (admin role mandatory)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "User")
    })
    @GetMapping("/{userId}")
    @PreAuthorize("hasPermission('USERS', 'READ')")
    public User getUser(@PathVariable("userId") Long userId) {
        return userRepository.findById(userId).orElseThrow();
    }

    @PostMapping("")
    @Operation(summary = "Add a new user", description = "Add a new user")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "User")
    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<User> addUser(@RequestBody UserRequest userRequest) {
        if (!StringUtils.isEmpty(userRequest.getPassword()) && !StringUtils.isEmpty(userRequest.getConfirmPassword())) {
            if (!userRequest.getPassword().equals(userRequest.getConfirmPassword())) {
                throw new GeneralException("Password and Confirm Password are not equal");
            }
        }
        if (StringUtils.isEmpty(userRequest.getUsername())) {
            throw new GeneralException("Username is mandatory");
        }
        if (StringUtils.isEmpty(userRequest.getEmail())) {
            throw new GeneralException("Email is mandatory");
        }
        if (userRepository.findByUsername(userRequest.getUsername()).isPresent()) {
            throw new GeneralException("User already exists");
        }
        User user = new User();
        user.setUsername(userRequest.getUsername());
        user.setEmail(userRequest.getEmail());
        user.setPassword(encoder.encode(userRequest.getPassword()));
        user = userRepository.save(user);
        commonAuthEventPublisher.publishUserUpdatedEvent(user.getId());
        return updateUser(user.getId(), userRequest);
    }

    @PutMapping("/{userId}")
    @Operation(summary = "Update user information", description = "Update user information")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Updated user")
    })
    public ResponseEntity<User> updateUser(@PathVariable("userId") Long userId, @RequestBody UserRequest userRequest) {
        if (!StringUtils.isEmpty(userRequest.getPassword()) && !StringUtils.isEmpty(userRequest.getConfirmPassword())) {
            if (!userRequest.getPassword().equals(userRequest.getConfirmPassword())) {
                throw new GeneralException("Password and Confirm Password are not equal");
            }
        }
        User user = userRepository.findById(userId).orElseThrow();

        if (!StringUtils.isEmpty(userRequest.getEmail())) {
            user.setEmail(userRequest.getEmail());
        }
        if (Optional.ofNullable(userRequest.getFirstName()).isPresent()) {
            user.setFirstName(userRequest.getFirstName());
        }
        if (Optional.ofNullable(userRequest.getLastName()).isPresent()) {
            user.setLastName(userRequest.getLastName());
        }
        if (Optional.ofNullable(userRequest.getTitle()).isPresent()) {
            user.setTitle(userRequest.getTitle());
        }
        if (Optional.ofNullable(userRequest.getTelephone()).isPresent()) {
            user.setTelephone(userRequest.getTelephone());
        }
        if (userRequest.getEnabled() != null) {
            user.setEnabled(userRequest.getEnabled());
        }
        if (userRequest.getResource() != null) {
            user.setResource(userRequest.getResource());
        }
        if (Optional.ofNullable(userRequest.getPicture()).isPresent()) {
            user.setPicture(userRequest.getPicture());
        }
        if (Optional.ofNullable(userRequest.getGender()).isPresent()) {
            user.setGender(userRequest.getGender());
        }
        if (Optional.ofNullable(userRequest.getAddress()).isPresent()) {
            user.setAddress(userRequest.getAddress());
        }
        if (Optional.ofNullable(userRequest.getAddress1()).isPresent()) {
            user.setAddress1(userRequest.getAddress1());
        }
        if (Optional.ofNullable(userRequest.getDetails()).isPresent()) {
            user.setDetails(userRequest.getDetails());
        }
        if (Optional.ofNullable(userRequest.getDetails1()).isPresent()) {
            user.setDetails1(userRequest.getDetails1());
        }
        if (Optional.ofNullable(userRequest.getExtras()).isPresent()) {
            user.setExtras(userRequest.getExtras());
        }
        if (Optional.ofNullable(userRequest.getSignature()).isPresent()) {
            user.setSignature(userRequest.getSignature());
        }
        if (userRequest.getRoleIds() != null) {
            user.setRoles(new HashSet<>(roleRepository.findAllById(userRequest.getRoleIds())));
        }
        if (!StringUtils.isEmpty(userRequest.getPassword())) {
            user.setPassword(encoder.encode(userRequest.getPassword()));
        }
        if (Optional.ofNullable(userRequest.getOrder1()).isPresent()) {
            user.setOrder1(userRequest.getOrder1());
        }
        if (Optional.ofNullable(userRequest.getOrder2()).isPresent()) {
            user.setOrder2(userRequest.getOrder2());
        }
        if (Optional.ofNullable(userRequest.getPinCode()).isPresent()) {
            user.setPinCode(userRequest.getPinCode());
        }
        validateUserFields(user);
        user = userRepository.save(user);
        // special for departments - this call should no happen - except the case when something was changed from drop down list and in this case
        // the value of costAllocationPercent will be wiped out
        if (userRequest.getDepartmentIds() != null && departmentsAreChanged(userRequest.getDepartmentIds(), user.getUserDepartments())) {
            List<UserDepartmentDTO> dtos = userRequest.getDepartmentIds().stream().map(depId -> {
                UserDepartmentDTO userDepartmentDTO = new UserDepartmentDTO();
                userDepartmentDTO.setUserId(userId);
                userDepartmentDTO.setDepartmentId(depId);
                userDepartmentDTO.setCostAllocationPercent(0);
                return userDepartmentDTO;
            }).toList();
            userService.updateDepartments(userId, dtos);
        }
        commonAuthEventPublisher.publishUserUpdatedEvent(user.getId());
        return ResponseEntity.ok(user);
    }

    private boolean departmentsAreChanged(List<Long> departmentIds, List<UserDepartment> userDepartments) {
        if (userDepartments == null) {
            return true;
        }
        List<Long> existingDepartmentIds = new ArrayList<>(userDepartments.stream().map(ud -> ud.getDepartment().getId()).toList());
        if (existingDepartmentIds.size() != departmentIds.size()) {
            return true;
        }
        existingDepartmentIds.removeIf(departmentIds::contains);
        return !existingDepartmentIds.isEmpty();
    }

    private void validateUserFields(User user) {
        if (user.getOrder1() == null) {
            user.setOrder1(0);
        }
        if (user.getOrder2() == null) {
            user.setOrder2(0);
        }
    }

    @PutMapping("/invalidate/{userId}")
    @Operation(summary = "Disable user", description = "Disable user (admin role mandatory)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "User has been disabled")
    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<MessageResponse> invalidateUser(@PathVariable("userId") Long userId) {
        User user = userRepository.findById(userId).orElseThrow();
        user.setEnabled(false);
        userRepository.save(user);
        commonAuthEventPublisher.publishUserUpdatedEvent(user.getId());
        return ResponseEntity.ok(new MessageResponse("User has been disabled"));
    }

    @DeleteMapping("/{userId}")
    @Operation(summary = "Delete user", description = "Delete the user (admin role mandatory)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "User has been deleted")
    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<MessageResponse> deleteUser(@PathVariable("userId") Long userId) {
        User user = userRepository.findById(userId).orElseThrow();
        userRepository.delete(user);
        commonAuthEventPublisher.publishUserUpdatedEvent(user.getId());
        return ResponseEntity.ok(new MessageResponse("User has been deleted"));
    }

    @PostMapping(value = "/filtered", params = {"page", "size"})
    @Operation(summary = "Get users", description = "List of users (paged) (admin role mandatory)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List of users (paged)")
    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Page<User> findFiltered(@RequestParam(name = "page", defaultValue = "0", required = false) Integer page,
        @RequestParam(name = "size", defaultValue = "100", required = false) Integer size, @RequestBody UserFilterDTO filterDTO) {
        Page<User> paged = userRepositoryPagingAndSorting.findFiltered(filterDTO, JPAUtils.calculatePageRequest(filterDTO.getSort(), page, size));
        paged.getContent().forEach(user -> user.reduce());
        return paged;
    }

    @PutMapping("/reorder")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Operation(summary = "Reorder", description = "Update")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "the updated items")
    })
    @Transactional
    public ResponseEntity<List<User>> reorder(@RequestBody List<UserReorderDTO> dtoList) {
        List<User> returnList = new ArrayList<>();
        dtoList.forEach(dto -> {
            User user = userRepository.findById(dto.getId()).orElseThrow();
            if (dto.getOrder1() != null) {
                user.setOrder1(dto.getOrder1());
            }
            if (dto.getOrder2() != null) {
                user.setOrder2(dto.getOrder2());
            }
            userRepository.save(user);
            returnList.add(user);
        });
        return ResponseEntity.ok(returnList);

    }
  /*
    -------------------- DEPARTMENT -----------------
  */

    @PutMapping("/{id}/department/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(value = "Update departments from list", response = User.class)
    @Operation(summary = "Update departments from list", description = "Update from list")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "the User")
    })
    public ResponseEntity<User> updateDepartmentList(@PathVariable Long id, @RequestBody List<UserDepartmentDTO> list) {
        return ResponseEntity.ok(userService.updateDepartments(id, list));
    }

    /*
    -------------------- Logger -------------
     */

    @GetMapping("/loggedUsers")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(value = "Get user activity information", response = UserActivities.class)
    @Operation(summary = "Get user activity information", description = "Get user activity information")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "User activities")
    })
    public ResponseEntity<Map<String, UserActivities>> getLoggedUsers() {
        return ResponseEntity.ok(activeUserStore.getUserActivities());
    }

    @GetMapping("/loggedUsers/{minutes}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(value = "Get user activity information for last x minutes", response = UserActivities.class)
    @Operation(summary = "Get user activity information for last x minutes", description = "Get user activity information for last x minutes")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "User activities for last x minutes")
    })
    public ResponseEntity<Map<String, UserActivities>> getLoggedUsers(@PathVariable Integer minutes) {
        return ResponseEntity.ok(activeUserStore.getActivitiesFromLastXMinutes(minutes));
    }
}
