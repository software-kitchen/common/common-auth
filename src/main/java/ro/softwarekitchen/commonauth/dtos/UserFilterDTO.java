package ro.softwarekitchen.commonauth.dtos;

import lombok.Data;

import java.util.List;

@Data
public class UserFilterDTO {
    private String username;

    private String firstName;

    private String lastName;

    private String telephone;

    private String email;

    private String gender;

    private String address;

    private SortDTO sort;

    private String generalField;

    private List<Long> departmentIds;
}
