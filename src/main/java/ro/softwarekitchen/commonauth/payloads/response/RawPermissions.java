package ro.softwarekitchen.commonauth.payloads.response;

import lombok.Data;

@Data
public class RawPermissions {
    private String module;

    private boolean rightToCreate;

    private boolean rightToRead;

    private boolean rightToWrite;

    private boolean rightToDelete;

}
