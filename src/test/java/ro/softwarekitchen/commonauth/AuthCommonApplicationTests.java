package ro.softwarekitchen.commonauth;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import ro.softwarekitchen.commonauth.models.TestEntity;
import ro.softwarekitchen.commonauth.repository.TestEntityRepository;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
    classes = CommonAuthApplication.class)
@AutoConfigureMockMvc
@Sql("/data-h2.sql")
@ActiveProfiles("test")
class HdpServiceApplicationTests {

    @MockBean
    JavaMailSender javaMailSender;

    @Autowired
    TestEntityRepository testEntityRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void jpaTest() {
        testEntityRepository.deleteAll();
        TestEntity testEntity = new TestEntity();
        testEntity.setName("Test");
        testEntityRepository.save(testEntity);
        TestEntity found = testEntityRepository.findByName("Test").orElseThrow(() -> new RuntimeException("Entity not saved !"));
        // then
        assertThat(found.getName()).isEqualTo(testEntity.getName());
    }

}