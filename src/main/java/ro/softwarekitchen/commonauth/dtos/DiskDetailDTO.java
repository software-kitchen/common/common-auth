package ro.softwarekitchen.commonauth.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DiskDetailDTO {
    private String path;

    private Long totalInGB;

    private Long usedInGB;

    private Long availableInGB;
}
