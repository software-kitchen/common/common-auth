package ro.softwarekitchen.commonauth.payloads.request;

import lombok.Data;
import org.springframework.lang.Nullable;

import java.util.List;

@Data
public class UserRequest {
    @Nullable
    String confirmPassword;

    @Nullable
    private String username;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String title;

    @Nullable
    private String telephone;

    @Nullable
    private Boolean enabled;

    @Nullable
    private Boolean resource;

    @Nullable
    private String picture;

    @Nullable
    private String gender;

    @Nullable
    private String address;

    @Nullable
    private String address1;

    @Nullable
    private String details;

    @Nullable
    private String details1;

    @Nullable
    private String extras;

    @Nullable
    private String signature;

    @Nullable
    private List<Long> roleIds;

    @Nullable
    private List<Long> departmentIds;

    @Nullable
    private String password;

    // order purpose
    @Nullable
    private Integer order1;

    @Nullable
    private Integer order2;

    @Nullable
    private String pinCode;
}
