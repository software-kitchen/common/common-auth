package ro.softwarekitchen.commonauth.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.softwarekitchen.commonauth.models.Department;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {

}

