package ro.softwarekitchen.commonauth.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = "username"),
        @UniqueConstraint(columnNames = "email")
    })
@Data
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 20)
    private String username;

    @Size(max = 255)
    @Column(name = "first_name")
    private String firstName;

    @Size(max = 244)
    @Column(name = "last_name")
    private String lastName;

    @Size(max = 244)
    @Column(name = "title")
    private String title;

    @Size(max = 50)
    private String telephone;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(max = 120)
    @JsonIgnore
    private String password;

    @Column(name = "reset_password_token")
    private String resetPasswordToken;

    private boolean enabled = true;

    // if the user is used as resource (planning)
    private boolean resource = true;

    @Size(max = 255)
    private String picture;

    @Size(max = 50)
    private String gender;

    @Size(max = 255)
    private String address;

    @Size(max = 255)
    private String address1;

    @Size(max = 4096)
    private String details;

    @Size(max = 4096)
    private String details1;

    @Lob
    @Column(length = 65535)
    private String extras;

    @Lob
    @Column(length = 65535)
    private String signature;

    // order purpose
    private Integer order1;

    private Integer order2;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
        joinColumns = @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @OneToMany(
        cascade = {CascadeType.ALL},
        fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @JsonManagedReference
    private List<UserDepartment> userDepartments;

    @Column(name = "api_key")
    private String apiKey;

    @Column(name = "pin_code")
    private String pinCode;

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public User reduce() {
        this.signature = null;
        this.password = null;
        this.roles = null;
        this.resetPasswordToken = null;
        return this;
    }

    // sample how to use a bean
    //
    //    public String getLeader() {
    //        if (this.leaderId == null) {
    //            return "NA";
    //        }
    //        UserRepository userRepository = (UserRepository) CommonAuthApplicationConfiguration.contextProvider().getApplicationContext().getBean("userRepository");
    //        Optional<User> leader = userRepository.findById(this.leaderId);
    //        if (leader.isPresent()) {
    //            return leader.get().getFirstName() + " " + leader.get().getLastName();
    //        } else {
    //            return "NA";
    //        }
    //    }
}
