package ro.softwarekitchen.commonauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.softwarekitchen.commonauth.models.ModulePermissions;

@Repository
public interface ModulePermissionsRepository extends JpaRepository<ModulePermissions, Long> {
}
