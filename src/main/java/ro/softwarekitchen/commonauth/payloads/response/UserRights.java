package ro.softwarekitchen.commonauth.payloads.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class UserRights {
    private List<Permissions> permissions;

    private String extras;
}
