package ro.softwarekitchen.commonauth.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "module_permissions")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModulePermissions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "module_id", referencedColumnName = "id")
    private Module module;

    @Column(name = "right_to_create")
    private boolean rightToCreate;

    @Column(name = "right_to_read")
    private boolean rightToRead;

    @Column(name = "right_to_write")
    private boolean rightToWrite;

    @Column(name = "right_to_delete")
    private boolean rightToDelete;
}
