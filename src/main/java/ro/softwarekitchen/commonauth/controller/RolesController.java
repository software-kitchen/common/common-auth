package ro.softwarekitchen.commonauth.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.softwarekitchen.commonauth.dtos.RoleFilterDTO;
import ro.softwarekitchen.commonauth.models.Role;
import ro.softwarekitchen.commonauth.payloads.request.RoleModulePermissionsRequest;
import ro.softwarekitchen.commonauth.payloads.request.RoleRequest;
import ro.softwarekitchen.commonauth.payloads.response.MessageResponse;
import ro.softwarekitchen.commonauth.repository.RoleRepository;
import ro.softwarekitchen.commonauth.repository.RoleRepositoryPagingAndSorting;
import ro.softwarekitchen.commonauth.services.RoleService;

@RestController
@RequestMapping("/roles")
@AllArgsConstructor
public class RolesController {
    private final RoleRepository roleRepository;

    private final RoleService roleService;

    private final RoleRepositoryPagingAndSorting roleRepositoryPagingAndSorting;

    @PostMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Operation(summary = "Create a new role", description = "Create a new role (admin role mandatory)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "New role")
    })
    public ResponseEntity<?> create(@Valid @RequestBody RoleRequest roleRequest) {
        if (roleRepository.findByName(roleRequest.getName()).isPresent()) {
            return ResponseEntity.badRequest().body(new MessageResponse("Role is already defined"));
        }
        Role role = new Role();
        role.setName(roleRequest.getName());
        if (roleRequest.getModulePermissions() != null) {
            role.setModulePermissions(roleRequest.getModulePermissions());
        }

        return ResponseEntity.ok(roleRepository.save(role));
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Operation(summary = "Get roles", description = "List of roles (admin role mandatory)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List of roles")
    })
    public ResponseEntity<?> getAllRoles() {
        return ResponseEntity.ok(roleRepository.findAll());
    }

    @GetMapping("/byName/{roleName}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Operation(summary = "Get role by name", description = "Get role by name(admin role mandatory)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Role by name")
    })
    public ResponseEntity<?> getByName(@PathVariable String roleName) {
        return ResponseEntity.ok(
            roleRepository.findByName(roleName).orElseThrow(() -> new RuntimeException("Error: Role: " + roleName + " is not found.")));
    }

    @GetMapping("/{roleId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Operation(summary = "Get role by id", description = "Get role by id (admin role mandatory)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Get role by id")
    })
    public ResponseEntity<?> get(@PathVariable Long roleId) {
        return ResponseEntity.ok(
            roleRepository.findById(roleId).orElseThrow(() -> new RuntimeException("Error: Role with id: " + roleId + " is not found.")));
    }

    @PostMapping(value = "/filtered", params = {"page", "size"})
    @Operation(summary = "Get roles", description = "List of roles (paged) (admin role mandatory)")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List of roles (paged)")
    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Page<Role> findAllPaged(@RequestParam(name = "page", defaultValue = "0", required = false) Integer page,
        @RequestParam(name = "size", defaultValue = "100", required = false) Integer size, @RequestBody RoleFilterDTO roleFilterDTO) {
        return roleRepositoryPagingAndSorting.findFiltered(roleFilterDTO, PageRequest.of(page, size));
    }

    @PutMapping()
    @Operation(summary = "Update role", description = "Updates role and role permissions")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Updated role and role permissions")
    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> updatePermissions(@RequestBody RoleModulePermissionsRequest rmpRequest) {
        return ResponseEntity.ok(roleService.updatePermissions(rmpRequest).orElseThrow());
    }
}
