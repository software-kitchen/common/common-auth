package ro.softwarekitchen.commonauth.config;

import com.corundumstudio.socketio.SocketIOServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class SocketIOConfig {

    // https://medium.com/folksdev/spring-boot-netty-socket-io-example-3f21fcc1147d

    @Value("${application.socket-server.host:localhost}")
    private String host;

    @Value("${application.socket-server.port:1973}")
    private Integer port;

    @Value("${application.localDev:false}")
    private boolean isLocalDev;

    @Bean
    public SocketIOServer socketIOServer() {
        com.corundumstudio.socketio.Configuration config = new com.corundumstudio.socketio.Configuration();
        config.setHostname(host);
        config.setPort(port);
        if (isLocalDev) {
            config.setOrigin("http://localhost:4200");
            config.setAllowCustomRequests(true);
        }

        return new SocketIOServer(config);
    }

}
