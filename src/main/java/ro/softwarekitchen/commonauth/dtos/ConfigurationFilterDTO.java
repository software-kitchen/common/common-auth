package ro.softwarekitchen.commonauth.dtos;

import lombok.Data;

@Data
public class ConfigurationFilterDTO {

    private String confKey;

}
