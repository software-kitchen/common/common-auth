package ro.softwarekitchen.commonauth.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableConfigurationProperties({WebConfigProperties.class})
public class WebCorsConfiguration {
    private final WebConfigProperties webConfigProperties;

    public WebCorsConfiguration(WebConfigProperties webConfigProperties) {
        this.webConfigProperties = webConfigProperties;
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                WebConfigProperties.Cors cors = webConfigProperties.getCors();
                registry.addMapping("/**")
                        .allowedOrigins(cors.getAllowedOrigins())
                        .allowedMethods(cors.getAllowedMethods())
                        //.allowedHeaders(cors.getAllowedHeaders())
                        .allowCredentials(cors.isAllowCredentials())
                        .maxAge(cors.getMaxAge());
                        //.exposedHeaders(cors.getExposedHeaders());

            }
        };
    }
}
