package ro.softwarekitchen.commonauth.security.users;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;
import java.util.Map;

@Data
@AllArgsConstructor
public class UserActivities {
    Map<UserRequest, Instant> requests;
}
