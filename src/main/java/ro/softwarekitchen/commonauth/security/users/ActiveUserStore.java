package ro.softwarekitchen.commonauth.security.users;

import lombok.Data;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

@Data
public class ActiveUserStore {
    private Map<String, UserActivities> userActivities;

    public ActiveUserStore() {
        userActivities = new HashMap<>();
    }

    public Map<String, UserActivities> getActivitiesFromLastXMinutes(int minutes) {
        Instant cutTime = Instant.now().minus(minutes, ChronoUnit.MINUTES);
        ;

        Map<String, UserActivities> returnMap = new HashMap<>();
        for (Map.Entry<String, UserActivities> entry : userActivities.entrySet()) {
            for (Map.Entry<UserRequest, Instant> request : entry.getValue().getRequests().entrySet()) {
                if (request.getValue().isAfter(cutTime)) {
                    UserActivities userActivities = returnMap.get(entry.getKey());
                    if (userActivities == null) {
                        userActivities = new UserActivities(new HashMap<>());
                    }
                    userActivities.getRequests().put(request.getKey(), request.getValue());
                    returnMap.put(entry.getKey(), userActivities);
                }
            }
        }
        return returnMap;
    }
}
