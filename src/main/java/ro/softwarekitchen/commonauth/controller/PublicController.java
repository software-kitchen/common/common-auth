package ro.softwarekitchen.commonauth.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/public")
public class PublicController {

    @GetMapping
    @Operation(summary = "App info", description = "App info")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List")
    })
    public ResponseEntity<Resource> getSKImage() throws IOException {
        ClassPathResource x = new ClassPathResource("Software-Kitchen-Logo.png");
        return ResponseEntity.ok()
            .contentType(MediaType.IMAGE_PNG)
            .body(x);
    }

}
