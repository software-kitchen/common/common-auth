package ro.softwarekitchen.commonauth.services;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.softwarekitchen.common_core.exception.ItemNotFoundException;
import ro.softwarekitchen.commonauth.dtos.UserDepartmentDTO;
import ro.softwarekitchen.commonauth.models.Department;
import ro.softwarekitchen.commonauth.models.User;
import ro.softwarekitchen.commonauth.models.UserDepartment;
import ro.softwarekitchen.commonauth.repository.DepartmentRepository;
import ro.softwarekitchen.commonauth.repository.UserDepartmentRepository;
import ro.softwarekitchen.commonauth.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserDepartmentRepository userDepartmentRepository;

    private final DepartmentRepository departmentRepository;

    private final PasswordEncoder encoder;

    @Override
    @Transactional
    public void updateResetPasswordToken(String token, String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            user.setResetPasswordToken(token);
            userRepository.save(user);
        } else {
            throw new UsernameNotFoundException("Could not find any user with the email " + email);
        }
    }

    @Override
    @Transactional
    public User getByResetPasswordToken(String token) {
        return userRepository.findByResetPasswordToken(token);
    }

    @Override
    @Transactional
    public void updatePassword(User user, String newPassword) {
        user.setPassword(encoder.encode(newPassword));
        user.setResetPasswordToken(null);
        userRepository.save(user);
    }

    @Override
    public User getCurrentUser() {
        String userName = Optional.ofNullable(SecurityContextHolder.getContext())
            .map(SecurityContext::getAuthentication)
            .filter(Authentication::isAuthenticated)
            .map(Authentication::getPrincipal)
            .map(UserDetailsImpl.class::cast)
            .map(UserDetailsImpl::getUsername).orElseThrow();
        return userRepository.findByUsername(userName).orElseThrow();
    }

    @Override
    public User updateDepartments(Long userId, List<UserDepartmentDTO> list) {
        User user = userRepository.findById(userId).orElseThrow(ItemNotFoundException::new);

        if (user.getUserDepartments() == null) {
            user.setUserDepartments(new ArrayList<>());
        }

        // remove all and add new ones
        List<Long> deptToBeDeleted = user.getUserDepartments().stream().map(UserDepartment::getId).toList();
        user.getUserDepartments().clear();
        userRepository.save(user);
        userDepartmentRepository.deleteAllById(deptToBeDeleted);

        list.forEach(ud -> {
            Optional<Department> opt = departmentRepository.findById(ud.getDepartmentId());
            if (opt.isPresent() && user.getUserDepartments().stream()
                .noneMatch(userDepartment -> userDepartment.getDepartment().getId() == ud.getDepartmentId())) {
                UserDepartment userDepartment = new UserDepartment();
                userDepartment.setUser(user);
                userDepartment.setDepartment(opt.get());
                userDepartment.setCostAllocationPercent(ud.getCostAllocationPercent());
                user.getUserDepartments().add(userDepartment);
            }
        });
        return userRepository.save(user);
    }
}
