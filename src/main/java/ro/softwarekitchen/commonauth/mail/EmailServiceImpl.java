package ro.softwarekitchen.commonauth.mail;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import ro.softwarekitchen.commonauth.config.mail.MailSenderProperties;

import java.io.File;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class EmailServiceImpl implements EmailService {

    private static final String MAIL_OK = "Mail sent Successfully";

    private static final String MAIL_ERROR = "Error while Sending Mail";

    private final JavaMailSender javaMailSender;

    private final TemplateEngine templateEngine;

    @Value("${spring.mail.sender}")
    private String sender;

    public String sendSimpleMail(EmailDetails details) {

        try {
            // send using simple message
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom(sender);
            mailMessage.setTo(parseTo(details.getRecipient()));
            mailMessage.setCc(parseTo(details.getRecipientCc()));
            mailMessage.setBcc(parseTo(details.getRecipientBcc()));
            mailMessage.setText(details.getMsgBody());
            mailMessage.setSubject(details.getSubject());

            javaMailSender.send(mailMessage);
            return MAIL_OK;
        } catch (Exception e) {
            log.error(MAIL_ERROR, e);
            return MAIL_ERROR + " - " + e.getMessage();
        }
    }

    @Async
    public void sendAsyncSimpleMail(EmailDetails details) {
        sendSimpleMail(details);
    }

    public String sendMail(EmailDetails details) {

        try {
            // send using mime message
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            helper.setFrom(sender);
            helper.setTo(parseTo(details.getRecipient()));
            helper.setCc(parseTo(details.getRecipientCc()));
            helper.setBcc(parseTo(details.getRecipientBcc()));
            helper.setSubject(details.getSubject());
            if (!StringUtils.isEmpty(details.getTemplateName())) {
                Context context = new Context();
                context.setVariable("message", details.getMsgBody());
                String htmlContent = templateEngine.process(details.getTemplateName(), context);
                helper.setText(htmlContent, true);
            } else {
                helper.setText(details.getMsgBody());
            }
            if (!StringUtils.isEmpty(details.getAttachment())) {
                FileSystemResource file = new FileSystemResource(
                    new File(details.getAttachment()));
                if (file.getFilename() != null) {
                    helper.addAttachment(file.getFilename(), file);
                }
            }
            javaMailSender.send(mimeMessage);
            return MAIL_OK;
        } catch (MessagingException e) {
            log.error(MAIL_ERROR, e);
            return MAIL_ERROR + " - " + e.getMessage();
        }

    }

    @Async
    public void sendAsyncMail(EmailDetails details) {
        sendMail(details);
    }

    public String sendHtmlEmailWithContext(EmailDetails details, Context context) {

        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            helper.setFrom(sender);
            helper.setTo(parseTo(details.getRecipient()));
            helper.setCc(parseTo(details.getRecipientCc()));
            helper.setBcc(parseTo(details.getRecipientBcc()));
            helper.setSubject(details.getSubject());
            String htmlContent = templateEngine.process(details.getTemplateName(), context);
            helper.setText(htmlContent, true);
            if (!StringUtils.isEmpty(details.getAttachment())) {
                FileSystemResource file = new FileSystemResource(
                    new File(details.getAttachment()));
                if (file.getFilename() != null) {
                    helper.addAttachment(file.getFilename(), file);
                }
            }
            javaMailSender.send(mimeMessage);
            return MAIL_OK;
        } catch (MessagingException e) {
            log.error(MAIL_ERROR, e);
            return MAIL_ERROR + " - " + e.getMessage();
        }
    }

    @Async
    public void sendAsyncHtmlEmailWithContext(EmailDetails details, Context context) {
        sendHtmlEmailWithContext(details, context);
    }

    @Override
    public MailSenderProperties getEmailProperties() {
        JavaMailSenderImpl sender = (JavaMailSenderImpl) javaMailSender;
        MailSenderProperties properties = new MailSenderProperties();

        properties.setHost(sender.getHost());
        properties.setProtocol(sender.getProtocol());
        properties.setPort(sender.getPort() + "");
        properties.setUsername(sender.getUsername());
        properties.setProps(sender.getJavaMailProperties());
        return properties;
    }

    /**
     * POST ...api/mail/sendHtmlMailExtended
     * {
     *   "recipient": "fgbogdan@gmail.com",
     *   "msgBody": "not good",
     *   "subject": "test subject",
     *   "templateName": "TemplateExtended",
     *   "attachments": [
     *   ],
     *   "inlineImages": [
     *     {
     *       "imageName":"family",
     *       "imageFile": "/home/bogdan/JAVA_WORK_SK/common/common-auth/src/main/resources/static/images/family.jpg"
     *     }
     *   ],
     *   "object": {
     *     "prop1":"prop2 of object"
     *   }
     * }
     */
    @Override
    public String sendHtmlEmailExtended(EmailDetailsExtended details) {
        try {
            // send using mime message
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            helper.setFrom(sender);
            helper.setTo(parseTo(details.getRecipient()));
            helper.setCc(parseTo(details.getRecipientCc()));
            helper.setBcc(parseTo(details.getRecipientBcc()));
            helper.setSubject(details.getSubject());
            if (!StringUtils.isEmpty(details.getTemplateName())) {
                Context context = new Context();
                context.setVariable("message", details.getMsgBody());
                // add object in context
                if(details.getObject()!=null){
                    context.setVariable("object", details.getObject());
                }
                String htmlContent = templateEngine.process(details.getTemplateName(), context);
                helper.setText(htmlContent, true);
            } else {
                helper.setText(details.getMsgBody());
            }
            if (details.getAttachments() != null) {
                details.getAttachments().forEach(attachment -> {
                    try {
                        FileSystemResource file = new FileSystemResource(new File(attachment));
                        if (file.getFilename() != null) {
                            helper.addAttachment(file.getFilename(), file);
                        }
                    } catch (Exception e) {
                        log.error("Failed to attach: {}", attachment, e);
                    }
                });
            }
            if (details.getInlineImages() != null) {
                details.getInlineImages().forEach(inlineImage -> {
                    try {
                        if (inlineImage != null) {
                            FileSystemResource file = new FileSystemResource(new File(inlineImage.getImageFile()));
                            helper.addInline(inlineImage.getImageName(), file);
                        }
                    } catch (Exception e) {
                        log.error("Failed to add inline image: {}", inlineImage.getImageFile(), e);
                    }
                });
            }
            javaMailSender.send(mimeMessage);
            return MAIL_OK;
        } catch (MessagingException e) {
            log.error(MAIL_ERROR, e);
            return MAIL_ERROR + " - " + e.getMessage();
        }
    }

    private String[] parseTo(String to) {
        if (to == null) {
            return new String[0];
        }
        return to.replace(",", ";").split(";");
    }
}